package no.hig.fiber.fiberApp;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.Context;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 *
 * Notification service that gets the newest article, video and radio show id's
 * It gets the a new list for each media and
 * If the id's ar not equal a notification i sent to the status bar notifying the user
 * that there is a new article, video or radio show
 *
 * Notification:
 * http://www.vogella.com/tutorials/AndroidNotifications/article.html#pendingintent
 * http://developer.android.com/guide/topics/ui/notifiers/notifications.html
 * http://developer.android.com/reference/android/support/v4/app/NotificationCompat.Builder.html
 *
 * IntentService:
 * http://it-ride.blogspot.no/2010/10/android-implementing-notification.html
 * http://code.tutsplus.com/tutorials/android-fundamentals-intentservice-basics--mobile-6183
 *
 */
public class NotificationService extends IntentService {

    AppFinalStrings strings = null;

    private final int ARTICLE_NOTIFICATION = 1;
    private final int VIDEO_NOTIFICATION = 2;
    private final int RADIO_NOTIFICATION = 3;

    private String currentLatestArticleId = null;
    private String currentLatestVideoId = null;
    private String currentLatestRadioId = null;

    public NotificationService() {
        super("NotificationService");
        strings = new AppFinalStrings(this.getApplicationContext());
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Sets data from intent and calls get data functions for each media
     * @param intent intent with article id, video id and radio show id
     */
    @Override
    protected void onHandleIntent(Intent intent) {

        // Gets articleID from the main application intent if there is some.
        currentLatestArticleId = intent.getStringExtra("articleId");
        currentLatestVideoId = intent.getStringExtra("videoId");
        //currentLatestRadioId = intent.getStringExtra("radioId");

        //getRadioIds(strings.fiberRadioPodcastRssUrl, currentLatestRadioId);
        getArticleIds(strings.fiberWordpressRssUrl, currentLatestArticleId);
        getVideoIds(strings.youtubeFiberTVJsonListUrl, currentLatestVideoId);
    }

    /*-------- RADIO NOTIFICATIONS --------*/

    /**
     * Gets xml data for radio shows checks the latest ID
     * @param url for getting radio data
     * @param id current newest radio id
     */
    void getRadioIds(String url, String id) {
        ArrayList<String> radioIds = new ArrayList<String>();
        Document XMLdoc = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;
        try {
            db = dbf.newDocumentBuilder();
            XMLdoc = db.parse(url);
        } catch (ParserConfigurationException e) {
            Log.d(strings.exceptionTagInLog, e.getMessage() );
        } catch (SAXException e) {
            Log.d(strings.exceptionTagInLog, e.getMessage() );
        } catch (IOException e) {
            Log.d(strings.exceptionTagInLog, e.getMessage() );
        }
        if (XMLdoc != null) {
            XMLdoc.getDocumentElement().normalize();
            NodeList itemNodes = XMLdoc.getElementsByTagName("item");  // Retrieve all the <item> nodes
            // Go through all the nodes to build items
            for (int i = 0; i < itemNodes.getLength(); i++) {
                Node itemNode = itemNodes.item(i);
                if (itemNode.getNodeType() == Node.ELEMENT_NODE) {
                    //---convert the Node into an Element---
                    Element itemElement = (Element) itemNode;
                    String tmpURL = itemElement.getElementsByTagName("guid").item(0).getTextContent();
                    String tmpID = tmpURL.split("/")[4];
                    radioIds.add(tmpID);
                }
            }

            // if the newest id from the xml is different from this latest stored id
            // a notification is sent to the status bar.
            if (!(id.equals((radioIds.get(0))))) {
                currentLatestRadioId = radioIds.get(0);
                sendNotification(
                        getResources().getString(R.string.radio),
                        getString(R.string.title_radio_notification),
                        getString(R.string.text_radio_notification),
                        RADIO_NOTIFICATION);
            }
        }
    }

    /*-------- VIDEO NOTIFICATION --------*/

    /**
     * Gets json data for videos and saves each id.
     * If there is a new video, makes a notification
     * @param url for getting video data
     * @param id current newest video id
     */
    void getVideoIds(String url, String id){
        String jsonFeed = getData(url);
        ArrayList<String> videoIds = new ArrayList<String>();
        try {
            JSONObject json = new JSONObject(jsonFeed);
            JSONArray jsonArray = json.getJSONArray("items");

            // Loop round our JSON list of videos creating Video objects to use within our app
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                JSONObject jsonObjectSnippet = jsonObject.getJSONObject("snippet");
                // The title of the video
                String tmpID = jsonObjectSnippet.getJSONObject("resourceId").getString("videoId");
                videoIds.add(tmpID);
            }
            // if the newest id from the xml is different from this newest id
            // a notification is sent to the status bar.
            if (!(id.equals((videoIds.get(0))))) {
                currentLatestVideoId = videoIds.get(0);
                sendNotification(
                        getResources().getString(R.string.tv),
                        getString(R.string.title_video_notification),
                        getString(R.string.text_video_notification), VIDEO_NOTIFICATION);
            }
        } catch (JSONException e) {
            Log.d(strings.exceptionTagInLog, e.getMessage() );
        }
    }

    /*-------- ARTICLE NOTIFICATION --------*/

    /**
     * Gets xml data for articles and saves each id.
     * If there is a new article, makes a notification
     * @param url for getting article data
     * @param id current newest article id
     */
    void getArticleIds(String url, String id) {
        String rssFeed = getData(url);
        ArrayList<String> articleIds = new ArrayList<String>();
        Document doc = null;
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;
        InputSource is = new InputSource();

        try {
            db = dbf.newDocumentBuilder();
            is.setCharacterStream(new StringReader(rssFeed));
            doc = db.parse(is);
        } catch (ParserConfigurationException e) {
            Log.d(strings.exceptionTagInLog, e.getMessage() );
        } catch (SAXException e) {
            Log.d(strings.exceptionTagInLog, e.getMessage() );
        } catch (IOException e) {
            Log.d(strings.exceptionTagInLog, e.getMessage() );
        }
        if (doc != null) {
            doc.getDocumentElement().normalize();
            NodeList itemNodes = doc.getElementsByTagName("item");  //---retrieve all the <item> nodes---

            for (int i = 0; i < itemNodes.getLength(); i++) {    // Go through all the nodes to build items
                Node itemNode = itemNodes.item(i);
                if (itemNode.getNodeType() == Node.ELEMENT_NODE) {

                    //---convert the Node into an Element---
                    Element itemElement = (Element) itemNode;

                    // Checking if the article is a frontpage article
                    boolean frontpage = false;
                    NodeList categoryNodes = itemElement.getElementsByTagName("category");
                    for (int j = 0; j < categoryNodes.getLength(); j++) {
                        String category = itemElement.getElementsByTagName("category").item(0).getTextContent();
                        if(category.equals("Forside"))
                            frontpage = true;
                    }
                    if(frontpage) {
                        String tmpID = itemElement.getElementsByTagName("guid").item(0).getTextContent();
                        tmpID = tmpID.split("=")[1];
                        articleIds.add(tmpID);
                    }
                }
            }
            // if the newest id from the xml is different from this newest id
            // a notification is sent to the status bar.
            if (!(id.equals((articleIds.get(0))))) {
                currentLatestArticleId = articleIds.get(0);
                sendNotification(
                        getResources().getString(R.string.newspaper),
                        getString(R.string.title_article_notification),
                        getString(R.string.text_article_notification),
                        ARTICLE_NOTIFICATION);
            }
        }
    }

    /*-------- GENERAL FUNCTIONS --------*/

    /**
     * Opens a url connection and gets the data
     * @param url for where to get data
     * @return the gotten data as a string
     */
    String getData(String url){
        String data = null;
        URL tmpUrl = null;
        try {
            tmpUrl = new URL(url);
        } catch (MalformedURLException e) {
            Log.d(strings.exceptionTagInLog, e.getMessage() );
        }
        try {
            URLConnection urlCon;
            if (tmpUrl != null) {
                urlCon = tmpUrl.openConnection();
                InputStream is = urlCon.getInputStream();
                BufferedInputStream bis = new BufferedInputStream(is);

                ByteArrayBuffer baf = new ByteArrayBuffer(262144);      // 256 KB
                int current;
                while ((current = bis.read()) != -1) {
                    baf.append((byte) current);
                }
                data = new String(baf.toByteArray(), "UTF8");
            }
        } catch (UnsupportedEncodingException e) {
            Log.d(strings.exceptionTagInLog, e.getMessage() );
        } catch (ClientProtocolException e) {
            Log.d(strings.exceptionTagInLog, e.getMessage() );
        } catch (IOException e) {
            Log.d(strings.exceptionTagInLog, e.getMessage() );
        }
        return data;
    }

    /**
     * Creates a notification for a type of media
     * @param actionType states the action id for which tab to open when user clicks on notification
     * @param title notification title
     * @param text notification text
     * @param notificationType the unique id for each media
     */
    void sendNotification(String actionType, String title, String text, int notificationType){
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        Intent notificationIntent = new Intent(this, FiberApplicationMain.class);
        notificationIntent.setAction(actionType);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_stat_notification)
                        .setContentTitle(title)
                        .setContentText(text)
                        .setAutoCancel(true);
        mBuilder.setContentIntent(pendingIntent);
        nm.notify(notificationType, mBuilder.build());
    }
}

