package no.hig.fiber.fiberApp;

import android.app.ActionBar;
import android.app.AlarmManager;
import android.app.FragmentTransaction;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import java.util.Calendar;


public class FiberApplicationMain extends FragmentActivity implements
        TabArticles.OnFirstArticleListener,
        TabVideo.OnFirstVideoListener
        //,TabRadio.OnFirstRadioListener
{

    private AppFinalStrings strings;
    private ViewPager Tab;
    private TabPagerAdapter TabAdapter = null;
    private ActionBar actionBar;
    private String articleId = null;
    private String videoId = null;
    private String radioId = null;

    private NotificationManager notificationManager;
    private AlarmManager alarmManager;
    private PendingIntent pendingIntent;
    private String exceptionTagInLog;
    private String InfoTagInLog;
    private Context appContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fiber_application_main_layout);
        appContext = getApplicationContext();
        // Updating strings from remote config update
        strings = new AppFinalStrings(getApplicationContext());
        strings.remoteConfigUpdate();
        exceptionTagInLog = strings.exceptionTagInLog;
        InfoTagInLog = strings.infoTagInLog;

        // Clears notification on app create
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        // Stops the alarmManager while fetching new data
        Intent intent = new Intent(this, NotificationService.class);
        pendingIntent = PendingIntent.getService(this, 0, intent, 0);
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
        }

        actionBar = getActionBar();


        if (TabAdapter == null)
            TabAdapter = new TabPagerAdapter(getSupportFragmentManager());
        Tab = (ViewPager) findViewById(R.id.pager);
        Tab.setOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener() {
                    /*Suppresses getSupportActionBar() warning*/

                    @Override
                    public void onPageSelected(int position) {
                        actionBar = getActionBar();
                        if (actionBar != null) {
                            actionBar.setSelectedNavigationItem(position);
                        }
                    }
                });
        Tab.setAdapter(TabAdapter);

        //Enable Tabs on Action Bar
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        final ActionBar.TabListener tabListener = new ActionBar.TabListener() {
            @Override
            public void onTabReselected(android.app.ActionBar.Tab Tab1,
                                        FragmentTransaction ft) {
            }

            @Override
            public void onTabSelected(ActionBar.Tab Tab2, FragmentTransaction ft) {
                Tab.setCurrentItem(Tab2.getPosition());

            }

            @Override
            public void onTabUnselected(android.app.ActionBar.Tab Tab3,
                                        FragmentTransaction ft) {

            }
        };
        //Add tabs
        actionBar.addTab(actionBar.newTab().setText(getResources().getString(R.string.tv)).setTabListener(tabListener));
        actionBar.addTab(actionBar.newTab().setText(getResources().getString(R.string.newspaper)).setTabListener(tabListener));
        //actionBar.addTab(actionBar.newTab().setText(getResources().getString(R.string.radio)).setTabListener(tabListener));

        // Selects video or radio tab if the user clicked a notification
        if (getIntent().getAction().equals(getResources().getString(R.string.tv))) {
            Tab.setCurrentItem(0, false);
        } else if (getIntent().getAction().equals(getResources().getString(R.string.radio))) {
            Tab.setCurrentItem(2, false);
        } else {
            /*Set second tab to show first. http://stackoverflow.com/questions/19454220/how-to-set-the-default-tab-of-multiple-swipe-views-with-tabs*/
            Tab.setCurrentItem(1, false);
        }

    }

    /**
     * Implements interface from TabRadio Fragment
     * If all all id's has value, starts navigation service
     * @param firstId - id of the newest radio show in list

     @Override public void getFirstRadioId(String firstId) {
     this.radioId = firstId;
     if((this.articleId != null)
     && (this.videoId != null)
     && (this.radioId != null)){
     startNavigationService(this.articleId, this.videoId, this.radioId);
     }

     } */

    /**
     * Implements interface from TabVideo Fragment
     * If all all id's has value, starts navigation service
     *
     * @param firstId - id of the newest video in list
     */
    @Override
    public void getFirstVideoId(String firstId) {
        this.videoId = firstId;
        if ((this.articleId != null)
                && (this.videoId != null)
                && (this.radioId != null)) {
            startNavigationService(this.articleId, this.videoId, this.radioId);
        }
    }

    /**
     * Implements interface from TabArticle Fragment
     * If all all id's has value, starts navigation service
     *
     * @param firstId - id of the newest article in list
     */
    @Override
    public void getFirstArticleId(String firstId) {
        this.articleId = firstId;
        if ((this.articleId != null)
                && (this.videoId != null)
                && (this.radioId != null)) {
            startNavigationService(this.articleId, this.videoId, this.radioId);
        }
    }

    /**
     * Sets an alarm managar that starts a notification service two times a day
     * The service gets the newest article, video and radio id's
     * PendingIntent:
     * http://stackoverflow.com/questions/4340431/how-can-i-correctly-pass-unique-extras-to-a-pending-intent
     * http://developer.android.com/reference/android/app/PendingIntent.html
     * AlarmManager:
     * http://navinsandroidtutorial.blogspot.no/2014/04/android-alarm-manager-service-and.html
     * https://developer.android.com/training/scheduling/alarms.html
     *
     * @param articleId newest article id
     * @param videoId   newest video id
     * @param radioId   newest radio show id
     */
    @SuppressWarnings("unused")
    void startNavigationService(String articleId, String videoId, String radioId) {
        Calendar cal = Calendar.getInstance();
        Intent intent = new Intent(this, NotificationService.class);
        intent.putExtra("radioId", radioId);
        intent.putExtra("articleId", articleId);
        intent.putExtra("videoId", videoId);
        pendingIntent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        // alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 15000, pendingIntent );
        alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, AlarmManager.INTERVAL_HOUR, AlarmManager.INTERVAL_HOUR, pendingIntent);
        // alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, AlarmManager.INTERVAL_HALF_DAY, AlarmManager.INTERVAL_HALF_DAY, pendingIntent);
    }


}