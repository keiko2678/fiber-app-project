package no.hig.fiber.fiberApp;

        import android.app.Activity;
        import android.content.Context;
        import android.content.Intent;
        import android.graphics.Color;
        import android.os.AsyncTask;
        import android.os.Bundle;
        import android.support.v4.app.Fragment;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.AdapterView;
        import android.widget.Button;
        import android.widget.ImageButton;
        import android.widget.ImageView;
        import android.widget.ListView;
        import android.widget.ProgressBar;

        import org.w3c.dom.Document;
        import org.w3c.dom.Element;
        import org.w3c.dom.Node;
        import org.w3c.dom.NodeList;
        import org.xml.sax.SAXException;

        import java.io.IOException;
        import java.util.ArrayList;
        import java.util.Collections;
        import java.util.Comparator;

        import javax.xml.parsers.DocumentBuilder;
        import javax.xml.parsers.DocumentBuilderFactory;
        import javax.xml.parsers.ParserConfigurationException;

@SuppressWarnings("unused")
public class TabRadio extends Fragment {
    AppFinalStrings strings = null;
    private String rssFeedUrl = strings.fiberRadioPodcastRssUrl;

    private ListView radioListViewRanked;
    private ListView radioListViewSorted;
    private RadioArrayAdapter sortedAdapter = null;
    private RadioArrayAdapter rankedAdapter = null;
    private ArrayList<RadioItem> radioList = new ArrayList<RadioItem>();
    private ArrayList<RadioItem> radioSortedList = new ArrayList<RadioItem>();
    private ArrayList<RadioItem> radioRankedList = new ArrayList<RadioItem>();
    private int noInShowingList = 0;
    private Document XMLdoc;
    private View radio = null;
    private boolean forceUpdate = false;
    private boolean showRanked = false;
    private OnFirstRadioListener listener;
    private ImageButton fiberHome;
    private Button latestButton;
    private Button popularButton;
    private View bckView;
    private ImageView bckImg;
    private ProgressBar bckProgress;
    private Context appContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        appContext = this.getActivity().getApplicationContext();
        strings = new AppFinalStrings(appContext);
        rssFeedUrl = strings.fiberRadioPodcastRssUrl;
        noInShowingList = strings.noOfItemsInTabLists;

       radio = inflater.inflate(R.layout.tab_radio_fragmentlayout, container, false);

        radioListViewRanked = (ListView) radio.findViewById(R.id.radio_list_view_sorted);
        rankedAdapter = new RadioArrayAdapter(radio.getContext(), R.id.radio_list_view_sorted, radioRankedList);
        radioListViewRanked.setAdapter(rankedAdapter);
        radioListViewRanked.setOnItemClickListener(item);

        radioListViewSorted = (ListView) radio.findViewById(R.id.radio_list_view_ranked);
        sortedAdapter = new RadioArrayAdapter(radio.getContext(), R.id.radio_list_view_ranked, radioSortedList);
        radioListViewSorted.setAdapter(sortedAdapter);
        radioListViewSorted.setOnItemClickListener(item);

        bckView = radio.findViewById(R.id.background);
        bckImg = (ImageView) radio.findViewById(R.id.background_logo);
        bckProgress = (ProgressBar) radio.findViewById(R.id.background_progressbar);

        fiberHome = (ImageButton) radio.findViewById(R.id.refresh_bottom);
        latestButton = (Button) radio.findViewById(R.id.latest_button);
        popularButton = (Button) radio.findViewById(R.id.popular_button);

        fiberHome.setOnClickListener(fiber);
        latestButton.setOnClickListener(latest);
        popularButton.setOnClickListener(popular);

        if(radioList.isEmpty()) {
            new BuildRadioList().execute(rssFeedUrl);
        }else {
            chooseList();
        }
        return radio;
    }

    /**
     * Interface for sending the newest article id to the connected Activity
     */
    public interface OnFirstRadioListener {
        public void getFirstRadioId(String firstId);
    }

    /**
     * Checks that the container activity has the interface
     * http://developer.android.com/training/basics/fragments/communicating.html
     * @param activity the connected activity
     */
    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        if (activity instanceof OnFirstRadioListener) {
            listener = (OnFirstRadioListener) activity;
        }

    }

    /**
     * Gets likes and dislikes in case of updates
     */
    @Override
    public void onResume(){
        super.onResume();
        if(!(radioSortedList.isEmpty())){
            getLikesDislikes();
        }
    }
 /* -------- ONCLICK LISTENERS -------- */

    /**
     * Anonymous function for onclick listener for the fiber button
     */
    private View.OnClickListener fiber = new View.OnClickListener()  {
        /**
         * Sets buttons and sorts list after latest items
         * @param view view
         */
        @Override
        public void onClick(View view){
            forceUpdate = true;
            showRanked = false;
            new BuildRadioList().execute(rssFeedUrl);
        }
    };

    /**
     * Anonymous function for onclick listener for the latest button
     */
    private View.OnClickListener latest = new View.OnClickListener()  {
        /**
         * Sets buttons and sorts list after latest items
         * @param view view
         */
        @Override
        public void onClick(View view){
            latestButton.setBackground(getResources().getDrawable(R.drawable.button_active));
            latestButton.setTextColor(getResources().getColor(R.color.dark_grey));
            popularButton.setBackgroundColor(Color.TRANSPARENT);
            //popularButton.setBackground(getResources().getDrawable(R.drawable.button));
            popularButton.setTextColor(getResources().getColor(R.color.white));
            showRanked = false;
            chooseList();
        }
    };

    /**
     * Anonymous function for onclick listener for the popular button
     */
    private View.OnClickListener popular = new View.OnClickListener() {
        /**
         * Sets buttons and sorts list after most popular items
         * @param view view
         */
        @Override
        public void onClick(View view){
            popularButton.setBackground(getResources().getDrawable(R.drawable.button_active));
            popularButton.setTextColor(getResources().getColor(R.color.dark_grey));
            latestButton.setBackgroundColor(Color.TRANSPARENT);
            //latestButton.setBackground(getResources().getDrawable(R.drawable.button));
            latestButton.setTextColor(getResources().getColor(R.color.white));
            showRanked = true;
            chooseList();
        }
    };

    private void UpdateRankedList(){
        radioRankedList.clear();
        for(int i = 0; i < radioList.size(); i++) {
            radioRankedList.add(radioList.get(i));
        }
        Collections.sort(radioRankedList, new customComparator());

        // If more than it is supposed to in the list, remove the latest
        while (radioRankedList.size() > noInShowingList) {
            radioRankedList.remove(noInShowingList);
        }

        for(int i = 0; i < radioRankedList.size(); i++){
            radioRankedList.get(i).StartImageDownloading();
        }
        rankedAdapter.notifyDataSetChanged();
    }

    private void chooseList(){
        bckView.setVisibility(View.INVISIBLE);
        bckImg.setVisibility(View.INVISIBLE);
        bckProgress.setVisibility(View.INVISIBLE);
        radioListViewSorted.setVisibility(View.INVISIBLE);
        radioListViewRanked.setVisibility(View.INVISIBLE);
        if(showRanked) {
            UpdateRankedList();
            radioListViewRanked.setVisibility(View.VISIBLE);
        }else{
            radioListViewSorted.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Task, responsible for downloading a RSS feed a given URL.
     * This task will execute the download asynchronously, and fill up the
     * UI field when ready.
     */

    private class BuildRadioList extends AsyncTask<String, Void, Document> {
        /**
         * Showing progress background while getting data
         */
        @Override
        protected void onPreExecute() {
            bckProgress.setVisibility(View.VISIBLE);
            bckImg.setVisibility(View.VISIBLE);
            bckView.setVisibility(View.VISIBLE);
            radioListViewRanked.setVisibility(View.INVISIBLE);
            radioListViewSorted.setVisibility(View.INVISIBLE);
            super.onPreExecute();
        }

        /**
         * We wanted it to get the raw xml file with all data.
         * http://stackoverflow.com/questions/13832039/download-xml-to-string
         *
         * @param url string URL
         * @return xml string structure
         */

        @Override
        protected Document doInBackground(String... url) {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db;
            if( XMLdoc == null || forceUpdate) {
                try {
                    db = dbf.newDocumentBuilder();
                    XMLdoc = db.parse(url[0]);
                } catch (ParserConfigurationException e) {
                    Log.d(strings.exceptionTagInLog, e.getMessage());
                } catch (SAXException e) {
                    Log.d(strings.exceptionTagInLog, e.getMessage());
                } catch (IOException e) {
                    Log.d(strings.exceptionTagInLog, e.getMessage());
                }
            }
            return XMLdoc;
        }


        @Override
        protected void onPostExecute(Document XMLdoc) {
            if(radioSortedList.isEmpty() || forceUpdate) {
                radioSortedList.clear();
                radioList.clear();

                if(XMLdoc != null) {
                    XMLdoc.getDocumentElement().normalize();
                    NodeList itemNodes = XMLdoc.getElementsByTagName("item");  //---retrieve all the <item> nodes---

                    for (int i = 0; i < itemNodes.getLength(); i++) {    // Go through all the nodes to build items
                        Node itemNode = itemNodes.item(i);
                        if (itemNode.getNodeType() == Node.ELEMENT_NODE) {

                            //---convert the Node into an Element---
                            Element itemElement = (Element) itemNode;

                            String tmpTitle = itemElement.getElementsByTagName("title").item(0).getTextContent();
                            String tmpURL = itemElement.getElementsByTagName("guid").item(0).getTextContent();
                            String tmpID = tmpURL.split("/")[4];

                            String[] date = itemElement.getElementsByTagName("pubDate").item(0).getTextContent().split(" ");
                            String tmpDate = date[1] + " " + date[2] + " " + date[3];

                            NodeList imageDataNodes = itemElement.getElementsByTagName("image");
                            Element imageDataElement = (Element) imageDataNodes.item(0);
                            String tmpImageURL = imageDataElement.getElementsByTagName("url").item(0).getTextContent();

                            RadioItem tmp = new RadioItem(i, tmpID, tmpTitle, tmpURL, tmpDate, tmpImageURL, sortedAdapter, rankedAdapter, appContext);
                            //Add the new item to the list
                            radioList.add(tmp);
                            if(i < noInShowingList){
                                radioSortedList.add(tmp);
                                tmp.StartImageDownloading();
                            }
                        }
                    }
                }
            }
            chooseList();
            forceUpdate = false;

            if(!radioSortedList.isEmpty()) {
                listener.getFirstRadioId((radioSortedList.get(0).ID));
            }
        }
    }

    /**
     * Gets like and dislike data for each item in radio list
     */
    private void getLikesDislikes() {
        for(RadioItem item : radioList) {
            item.new GetRadioLikeDislike().execute();
        }
    }

    private AdapterView.OnItemClickListener item = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, final View view, int position, long itemId) {
            RadioItem item;
            if(showRanked){
                item = radioRankedList.get(position);
            }else {
                item = radioSortedList.get(position);
            }
            Intent intent = new Intent(adapterView.getContext(), RadioActivity.class);
            intent.putExtra("itemID", item.ID);
            intent.putExtra("itemURL", item.url);
            intent.putExtra("likes", item.likes);
            intent.putExtra("dislikes", item.dislikes);
            startActivity(intent);
        }
    };


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /**
     * Comparator principle taken from the following code snippet.
     * http://stackoverflow.com/questions/2784514/sort-arraylist-of-custom-objects-by-property
     */
    private class customComparator implements Comparator<RadioItem> {
        public int compare(RadioItem ai1, RadioItem ai2) {
            return ((ai2.likes+ai2.dislikes)- (ai1.likes+ai1.dislikes));
        }
    }
}