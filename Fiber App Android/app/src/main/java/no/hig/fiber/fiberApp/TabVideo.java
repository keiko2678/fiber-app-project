package no.hig.fiber.fiberApp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;



public class TabVideo extends Fragment {
    AppFinalStrings strings = null;
    private String youtubeJsonListUrl = null;
    private ListView videoListViewSorted;
    private ListView videoListViewRanked;
    private VideoArrayAdapter sortedAdapter = null;
    private VideoArrayAdapter rankedAdapter = null;
    private ArrayList<VideoItem> videoList = new ArrayList<VideoItem>();
    private ArrayList<VideoItem> videoSortedList = new ArrayList<VideoItem>();
    private ArrayList<VideoItem> videoRankedList = new ArrayList<VideoItem>();
    private int noInShowingList = 5;
    private String jsonFeed = null;
    private View tv = null;
    private boolean forceUpdate = false;
    private boolean showRanked = false;
    private OnFirstVideoListener listener;
    private ImageButton fiberHome;
    private Button latestButton;
    private Button popularButton;
    private View bckView;
    private ImageView bckImg;
    private ProgressBar bckProgress;
    private Context appContext;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        appContext = this.getActivity().getApplicationContext();
        strings = new AppFinalStrings(appContext);
        youtubeJsonListUrl = strings.youtubeFiberTVJsonListUrl;
        noInShowingList = strings.noOfItemsInTabLists;


        tv = inflater.inflate(R.layout.tab_video_fragmentlayout, container, false);

        videoListViewRanked = (ListView) tv.findViewById(R.id.video_list_view_sorted);
        rankedAdapter = new VideoArrayAdapter(tv.getContext(), R.id.video_list_view_sorted, videoRankedList);
        videoListViewRanked.setAdapter(rankedAdapter);
        videoListViewRanked.setOnItemClickListener(item);

        videoListViewSorted = (ListView) tv.findViewById(R.id.video_list_view_ranked);
        sortedAdapter = new VideoArrayAdapter(tv.getContext(), R.id.video_list_view_ranked, videoSortedList);
        videoListViewSorted.setAdapter(sortedAdapter);
        videoListViewSorted.setOnItemClickListener(item);

        bckView = tv.findViewById(R.id.background);
        bckImg = (ImageView) tv.findViewById(R.id.background_logo);
        bckProgress = (ProgressBar) tv.findViewById(R.id.background_progressbar);

        chooseList();

        fiberHome = (ImageButton) tv.findViewById(R.id.refresh_bottom);
        latestButton = (Button) tv.findViewById(R.id.latest_button);
        popularButton = (Button) tv.findViewById(R.id.popular_button);

        fiberHome.setOnClickListener(fiber);
        latestButton.setOnClickListener(latest);
        popularButton.setOnClickListener(popular);

        if(videoList.isEmpty()) {
            new BuildVideoList().execute(youtubeJsonListUrl);
        }else {
            chooseList();
        }

        return tv;
    }

    /**
     * Interface for sending the newest video id to the connected Activity
     *
     */
    public interface OnFirstVideoListener {
        public void getFirstVideoId(String firstId);
    }
    /**
     * Checks that the container activity has the interface
     * http://developer.android.com/training/basics/fragments/communicating.html
     * @param activity the connected activity
     */
    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        if (activity instanceof OnFirstVideoListener) {
            listener = (OnFirstVideoListener) activity;
        } else {
            throw new ClassCastException(activity.toString()
                    + " must implement MyListFragment.OnItemSelectedListener");
        }

    }


    /**
     * Gets likes and dislikes in case of updates
     */
    @Override
    public void onResume(){
        super.onResume();
        if(!(videoSortedList.isEmpty())){
            getLikesDislikes();
        }
    }
     /* -------- ONCLICK LISTENERS -------- */

    /**
     * Anonymous function for onclick listener for the fiber button
     */
    private View.OnClickListener fiber = new View.OnClickListener()  {
        /**
         * Sets buttons and sorts list after latest items
         * @param view view
         */
        @Override
        public void onClick(View view){
            forceUpdate = true;
            showRanked = false;
            new BuildVideoList().execute(youtubeJsonListUrl);
        }
    };

    /**
     * Anonymous function for onclick listener for the latest button
     */
    private View.OnClickListener latest = new View.OnClickListener()  {
        /**
         * Sets buttons and sorts list after latest items
         * @param view view
         */
        @Override
        public void onClick(View view){
            latestButton.setBackground(getResources().getDrawable(R.drawable.button_active));
            latestButton.setTextColor(getResources().getColor(R.color.dark_grey));
            popularButton.setBackgroundColor(Color.TRANSPARENT);
            //popularButton.setBackground(getResources().getDrawable(R.drawable.button));
            popularButton.setTextColor(getResources().getColor(R.color.white));
            showRanked = false;
            chooseList();
        }
    };

    /**
     * Anonymous function for onclick listener for the popular button
     */
    private View.OnClickListener popular = new View.OnClickListener() {
        /**
         * Sets buttons and sorts list after most popular items
         * @param view view
         */
        @Override
        public void onClick(View view){
            popularButton.setBackground(getResources().getDrawable(R.drawable.button_active));
            popularButton.setTextColor(getResources().getColor(R.color.dark_grey));
            latestButton.setBackgroundColor(Color.TRANSPARENT);
            //latestButton.setBackground(getResources().getDrawable(R.drawable.button));
            latestButton.setTextColor(getResources().getColor(R.color.white));
            showRanked = true;
            chooseList();
        }
    };

    private void UpdateRankedList(){
        videoRankedList.clear();
        for(int i = 0; i < videoList.size(); i++) {
            videoRankedList.add(videoList.get(i));
        }
        Collections.sort(videoRankedList, new customComparator());

        // If more than it is supposed to in the list, remove the latest
        while (videoRankedList.size() > noInShowingList) {
            videoRankedList.remove(noInShowingList);
        }
        for(int i = 0; i < videoRankedList.size(); i++){
            videoRankedList.get(i).StartImageDownloading();
        }
        rankedAdapter.notifyDataSetChanged();
    }

    private void chooseList(){
        bckView.setVisibility(View.INVISIBLE);
        bckImg.setVisibility(View.INVISIBLE);
        bckProgress.setVisibility(View.INVISIBLE);
        videoListViewSorted.setVisibility(View.INVISIBLE);
        videoListViewRanked.setVisibility(View.INVISIBLE);
        if(showRanked) {
            UpdateRankedList();
            videoListViewRanked.setVisibility(View.VISIBLE);
        }else{
            videoListViewSorted.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Task, responsible for downloading a RSS feed a given URL.
     * This task will execute the download asynchronously, and fill up the
     * UI field when ready.
     */

    private class BuildVideoList extends AsyncTask<String, Void, String> {
        /**
         * Showing progress background while getting data
         */
        @Override
        protected void onPreExecute() {
            bckProgress.setVisibility(View.VISIBLE);
            bckImg.setVisibility(View.VISIBLE);
            bckView.setVisibility(View.VISIBLE);
            videoListViewRanked.setVisibility(View.INVISIBLE);
            videoListViewSorted.setVisibility(View.INVISIBLE);
            super.onPreExecute();
        }

        /**
         * We wanted it to get the raw xml file with all data.
         * http://stackoverflow.com/questions/13832039/download-xml-to-string
         *
         * @param url string URL
         * @return xml string structure
         */

        @Override
        protected String doInBackground(String... url) {
            if(jsonFeed == null || forceUpdate) {
                URL tmpUrl = null;
                try {
                    tmpUrl = new URL(url[0]);
                } catch (MalformedURLException e) {
                    Log.d(strings.exceptionTagInLog, e.getMessage() );
                }
                try {
                    URLConnection urlCon;
                    if (tmpUrl != null) {
                        urlCon = tmpUrl.openConnection();


                        InputStream is = urlCon.getInputStream();
                        BufferedInputStream bis = new BufferedInputStream(is);

                        ByteArrayBuffer baf = new ByteArrayBuffer(262144);      // 256 KB
                        int current;
                        while ((current = bis.read()) != -1) {
                            baf.append((byte) current);
                        }
                        jsonFeed = new String(baf.toByteArray(), "UTF8");
                    }
                } catch (UnsupportedEncodingException e) {
                    Log.d(strings.exceptionTagInLog, "UnsupportedEncodingException" + e.getMessage() );
                } catch (ClientProtocolException e) {
                    Log.d(strings.exceptionTagInLog, "ClientProtocolException" + e.getMessage() );
                } catch (IOException e) {
                    Log.d(strings.exceptionTagInLog, "IOException" + e.getMessage() );
                }
            }
            return jsonFeed;
        }


        @Override
        protected void onPostExecute(String jsn) {
            if(videoSortedList.isEmpty() || forceUpdate) {
                videoSortedList.clear();
                videoList.clear();

                try {

                    JSONObject json = new JSONObject(jsn);
                    if (json != null) {
                        JSONArray jsonArray = json.getJSONArray("items");



                        // Loop round our JSON list of videos creating Video objects to use within our app
                        for (int i = 0; i < jsonArray.length(); i++) {
                            String tmpImageURL;

                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            JSONObject jsonObjectSnippet = jsonObject.getJSONObject("snippet");

                            // The title of the video
                            String tmpID = jsonObjectSnippet.getJSONObject("resourceId").getString("videoId");
                            String tmpTitle = jsonObjectSnippet.getString("title");
                            String tmpDate = jsonObjectSnippet.getString("publishedAt");
                            String tmpDescription = jsonObjectSnippet.getString("description");
                            tmpDate = DateFix(tmpDate);
                            // The url link back to YouTube, this checks if it has a mobile url
                            // if it doesnt it gets the standard url
                            try {
                                tmpImageURL = jsonObjectSnippet.getJSONObject("thumbnails").getJSONObject("medium").getString("url");
                            } catch (JSONException ignore) {
                                tmpImageURL = jsonObjectSnippet.getJSONObject("thumbnails").getJSONObject("default").getString("url");
                            }
                            // A url to the thumbnail image of the video
                            // We will use this later to get an image using a Custom ImageView
                            // Found here http://blog.blundell-apps.com/imageview-with-loading-spinner/
                            String tmpPlaylistID = jsonObjectSnippet.getString("playlistId");
                            String tmpURL = "http://youtu.be/" + tmpID + "?list=" + tmpPlaylistID;

                            // Create the video object and add it to our list
                            VideoItem tmp = new VideoItem(tmpID, tmpTitle, tmpURL, tmpDate, tmpImageURL, sortedAdapter, rankedAdapter, tmpDescription, appContext);
                            videoList.add(tmp);
                            if(i < noInShowingList){
                                videoSortedList.add(tmp);
                                tmp.StartImageDownloading();
                            }
                        }
                    }
                } catch (NullPointerException e){
                    Log.d(strings.exceptionTagInLog, "No Internt Connection" );
                } catch (JSONException e) {
                    Log.d(strings.exceptionTagInLog, e.getMessage() );
                }

            }
            chooseList();
            forceUpdate = false;

            if(!videoSortedList.isEmpty()) {
                listener.getFirstVideoId((videoSortedList.get(0).ID));
            }
        }
    }


    /**
     * Gets like and dislike data for each item in video list
     */
    private void getLikesDislikes() {
        for(VideoItem item : videoList) {
            item.new GetVideoLikeDislike().execute();
        }
    }

    private AdapterView.OnItemClickListener item = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, final View view, int position, long itemId) {
            VideoItem item;
            if(showRanked){
                item = videoRankedList.get(position);
            }else {
                item = videoSortedList.get(position);
            }
            Intent intent = new Intent(adapterView.getContext(), VideoActivity.class);
            intent.putExtra("itemID", item.ID);
            intent.putExtra("itemURL", item.url);
            intent.putExtra("itemTitle", item.title);
            intent.putExtra("itemDescription", item.description);
            intent.putExtra("itemDate", item.date);
            intent.putExtra("likes", item.likes);
            intent.putExtra("dislikes", item.dislikes);
            startActivity(intent);
        }
    };


    private String DateFix (String inn){
        String[] months = {"","Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        String tmpYear = inn.split("-")[0];
        String tmpMonth = months[Integer.valueOf(inn.split("-")[1])];
        String tmpDay = (inn.split("-")[2]).split("T")[0];
        return (tmpDay + " " + tmpMonth + " " + tmpYear);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /**
     * Comparator principle taken from the following code snippet.
     * http://stackoverflow.com/questions/2784514/sort-arraylist-of-custom-objects-by-property
     */
    private class customComparator implements Comparator<VideoItem> {
        public int compare(VideoItem ai1, VideoItem ai2) {
            return ((ai2.likes+ai2.dislikes)- (ai1.likes+ai1.dislikes));
        }
    }
}


