package no.hig.fiber.fiberApp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import static no.hig.fiber.fiberApp.ViewHolderClass.*;

/**
 * A custom ArrayAdapter class for setting item values in TextViews
 *
 * http://www.vogella.com/tutorials/AndroidListView/article.html
 * http://developer.android.com/training/improving-layouts/smooth-scrolling.html#ViewHolder
 * http://stackoverflow.com/questions/13164054/viewholder-good-practice
 */
class RadioArrayAdapter extends ArrayAdapter<RadioItem> {

    private final Context context;
    private ArrayList<RadioItem> values = null;

 /*   private View itemInListView;
    private TextView title;
    private TextView date;
    private TextView likes;
    private TextView dislikes;
    private ImageView image;
*/
    public RadioArrayAdapter(Context context, int layoutResourceId, ArrayList<RadioItem> values) {
        super(context, layoutResourceId, values);
        this.context = context;
        this.values = values;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View itemInListView = convertView;
        ViewHolder holder;

        if(itemInListView == null){
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemInListView = inflater.inflate(R.layout.array_adapter_radio, parent, false);
            holder = new ViewHolder();
            holder.vImage = (ImageView) itemInListView.findViewById(R.id.image_in_list);
            holder.vTitle = (TextView) itemInListView.findViewById(R.id.title_in_list);
            holder.vDate = (TextView) itemInListView.findViewById(R.id.date_in_list);
            holder.vLikes = (TextView) itemInListView.findViewById(R.id.likes_in_list);
            holder.vDislikes = (TextView) itemInListView.findViewById(R.id.dislikes_in_list);

            itemInListView.setTag(holder);
        } else {
            holder = (ViewHolder) itemInListView.getTag();
        }
        holder.vTitle.setText(values.get(position).title);
        holder.vDate.setText(values.get(position).date);
        holder.vLikes.setText(context.getString(R.string.likes) + String.valueOf(values.get(position).likes));
        holder.vDislikes.setText(context.getString(R.string.dislikes) + String.valueOf(values.get(position).dislikes));

        if (values.get(position).picture != null)
            holder.vImage.setImageBitmap(values.get(position).picture);

        return itemInListView;

    }
}

