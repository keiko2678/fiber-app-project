package no.hig.fiber.fiberApp;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;


/**
 * Fragment that handles like and dislike data
 * and updates like and dislike fragment view
 * https://guides.codepath.com/android/Creating-and-Using-Fragments
 */
public class LikeDislikeFragment extends Fragment {

    private String ID;
    private String type;                    // A, V or P
    private int likes;
    private int dislikes;
    private int likePushed;                  // 1 if pushed, 0 if isn't pushed
    private int dislikePushed;               // 1 if pushed, 0 if isn't pushed

    private ApiHandler api;
    private DatabaseItem DBItem;
    private DatabaseHelper DBHelper;

    private ImageButton likeButton;
    private ImageButton dislikeButton;
    private ImageButton fiberButton;

    private TextView likesView;
    private TextView dislikesView;
    public boolean bVisible = false;
    private Context appContext;

    /**
     * Required empty public constructor
     */
    public LikeDislikeFragment() {}

    /**
     * A instance of the fragments with arguments that gets id and type from Activity
     * @param id id
     * @param type type of media
     * @return fragment
     */
    public static LikeDislikeFragment newInstance(String id, String type, int likes, int dislikes) {
        LikeDislikeFragment fragmentLD = new LikeDislikeFragment();
        Bundle args = new Bundle();
        args.putString("id", id);
        args.putString("type", type);
        args.putInt("likes", likes);
        args.putInt("dislikes", dislikes);
        fragmentLD.setArguments(args);
        return fragmentLD;
    }



    /**
     * Called when the Fragment instance is being created
     * Gets id and type from instance
     * Set likes and dislikes to 0
     * Set if pushed or not to 0
     * @param savedInstanceState saved state
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appContext = this.getActivity().getApplicationContext();

        this.ID = getArguments().getString("id");
        this.type= getArguments().getString("type");
        this.likes = getArguments().getInt("likes");
        this.dislikes = getArguments().getInt("dislikes");
        this.likePushed = 0;
        this.dislikePushed= 0;
        DBHelper = new DatabaseHelper(getActivity());
        api = new ApiHandler(this.ID, this.type, appContext);
        //  new GetDatabaseInfo().execute(DBHelper);
        new GetDatabaseInfo().execute(DBHelper);
        new GetLikeDislike().execute();
    }


    /**
     * Handles views and sets on click listeners to buttons
     * Gets like and dislike data from API
     * Gets if pushed or not data from local database
     * @param inflater to inflate views
     * @param container parent view
     * @param savedInstanceState saved state
     * @return Like and disklige Fragment View
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View fragView = inflater.inflate(R.layout.fragment_like_dislike, container, false);

        likesView = (TextView) fragView.findViewById(R.id.likes_in_fragment);
        dislikesView = (TextView) fragView.findViewById(R.id.dislikes_in_fragment);
        likesView.setText(String.valueOf(likes));
        dislikesView.setText(String.valueOf(dislikes));

        likeButton = (ImageButton) fragView.findViewById(R.id.like_button_fragment);
        dislikeButton = (ImageButton) fragView.findViewById(R.id.dislike_button_fragment);
        fiberButton = (ImageButton) fragView.findViewById(R.id.fiber_button_fragment);

        likeButton.setOnClickListener(like);
        dislikeButton.setOnClickListener(dislike);
        fiberButton.setOnClickListener(fiber);
        if(bVisible == false){
            buttonVisibility(false);
        } else {
             buttonVisibility(true);
             setButton();
        }
        // enables buttons until all data is gotten.
       // buttonVisibility(false);


        return fragView;

    }

    /**
     * Can search for activity views here
     * @param savedInstanceState saved state
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

     /*------- ON CLICK LISTENERS --------*/

    /**
     * Anonymous function for onclick listener for the like button
     */
    private View.OnClickListener like = new View.OnClickListener()  {
        /**
         * Calls AsyncTask that handles updating like and dislike count
         * @param view view
         */
        @Override
        public void onClick(View view){
            bVisible = false;
            buttonVisibility(bVisible);
            new updateLike().execute(api);

        }
    };

    /**
     * Anonymous function for onclick listener for the dislike button
     */
    private View.OnClickListener dislike = new View.OnClickListener() {
        /**
         * Calls AsyncTask that handles updating like and dislike count
         * @param view view
         */
        @Override
        public void onClick(View view){
            bVisible = false;
            buttonVisibility(bVisible);
            new updateDislike().execute(api);



        }
    };
    /**
     * Anonymous function for onclick listener for the dislike button
     */
    private View.OnClickListener fiber = new View.OnClickListener() {
        /**
         * Returns to main activity
         * @param view view
         */
        @Override
        public void onClick(View view){
            getActivity().finish();
        }
    };

    /*------- API FUNCTIONS --------*/

    /**
     * AsyncTask for handling getting like and dislike data from API
     */
    private class GetLikeDislike extends AsyncTask<Void,  Void, ApiHandler>{
        /**
         * Gets data from API via ApiHandler
         * @return api object with the new data
         */
        @Override
        protected ApiHandler doInBackground(Void... params) {
            api =  api.getLikeDislike();
            return api;
        }
        /**
         * Updates like and dislike view with new like and dislike data
         * @param api with new data
         */
        protected void onPostExecute(ApiHandler api) {
            likes = api.likes;
            dislikes = api.dislikes;
            likesView.setText(String.valueOf(likes));
            dislikesView.setText(String.valueOf(dislikes));
        }
    }

    /**
     * AsyncTask for updating like and dislike data via API
     */
    private class updateLike extends AsyncTask<ApiHandler,  Void, ApiHandler> {
        /**
         * Calls the api handlers update like function
         * @param api handler object
         * @return updated api handler object
         */
        @Override
        protected ApiHandler doInBackground(ApiHandler... api) {
           api[0] = api[0].updateLike(likePushed, dislikePushed);
           return api[0];
        }
        /**
         * Updates whats been pushed by the user
         * 1 if has been pushed, 0 if hasn't
         * Updates database with whats been pushed and not
         * Sets like and dislike buttons
         * Updates like and dislike count views
         * @param api handler object
         */
        protected void onPostExecute(ApiHandler api) {
            likePushed = (likePushed == 1) ? 0 : 1;
            if(dislikePushed == 1) {
                dislikePushed = 0;
            }
             new UpdateDatabaseInfo().execute(DBHelper);
            setButton();
            likes = api.likes;
            dislikes = api.dislikes;
            likesView.setText(String.valueOf(likes));
            dislikesView.setText(String.valueOf(dislikes));
        }
    }


    /**
     * AsyncTask for updating like and dislike data via API
     */
    private class updateDislike extends AsyncTask<ApiHandler,  Void, ApiHandler> {
        /**
         * Calls the api handlers update dislike function
         * @param api handler object
         * @return updated api handler object
         */
        @Override
        protected ApiHandler doInBackground(ApiHandler... api) {
            api[0] = api[0].updateDislike(likePushed, dislikePushed);
            return api[0];
        }

        /**
         * Updates whats been pushed by the user
         * 1 if has been pushed, 0 if hasn't
         * Updates database with whats been pushed and not
         * Sets like and dislike buttons
         * Updates like and dislike count views
         * @param api handler object
         */
        protected void onPostExecute(ApiHandler api) {
            dislikePushed = (dislikePushed == 1) ? 0 : 1;
            if(likePushed == 1) {
                likePushed = 0;
            }
            new UpdateDatabaseInfo().execute(DBHelper);
            setButton();
            likes = api.likes;
            dislikes = api.dislikes;
            likesView.setText(String.valueOf(likes));
            dislikesView.setText(String.valueOf(dislikes));

        }
    }

    /*-------- DATABASE FUNCTIONS --------*/

    /**
     * Async Task for getting data on one item from local database
     */
    private class GetDatabaseInfo extends AsyncTask<DatabaseHelper,  Void, DatabaseItem> {
        /**
         * Gets item from id and type
         * If item didn't exists, inserts new item in local database
         * @param DBHelper database helper
         * @return item from database or null
         */
        @Override
        protected DatabaseItem doInBackground(DatabaseHelper... DBHelper) {
            DBItem = new DatabaseItem();

            DBItem = DatabaseItem.getItem(DBHelper[0], ID, type);
            if (DBItem == null) {
                DatabaseItem newDBItem = new DatabaseItem(
                        ID, type, likePushed, dislikePushed);
                newDBItem.save(DBHelper[0]);
            }
            return DBItem;
        }
        /**
         * If item was found
         * updates whats pushed data from database
         * Sets like and dislike buttons
         * @param DBItem database item
         */
        protected void onPostExecute(DatabaseItem DBItem) {
            if (DBItem != null) {
                likePushed = DBItem.pushedLike;
                dislikePushed = DBItem.pushedDislike;
            }
            bVisible = true;
            buttonVisibility(bVisible);
            setButton();
        }
    }
    /**
     * Async Task for updating whats pushed in local database
     */
    private class UpdateDatabaseInfo extends AsyncTask<DatabaseHelper, Void, DatabaseItem> {
        /**
         * Updates local database with new pushed data
         * @param DBHelper Helping class
         * @return null DBItem
         */
        @Override
        protected DatabaseItem doInBackground(DatabaseHelper... DBHelper) {
            DBItem = new DatabaseItem(ID, type, likePushed, dislikePushed);
            DBItem.updateLikeDislike(DBHelper[0]);
            return DBItem;
        }
        protected void onPostExecute(DatabaseItem DBItem){
            bVisible = true;
            buttonVisibility(bVisible);
            setButton();
        }

    }

         /*-------- GENERAL FUNCTIONS --------*/

    /**
     * Changes image on like and dislike buttons based on whether
     *  they have been pushed or not
     */
    private void setButton() {
        if (likePushed == 1 ) {
            likeButton.setImageResource(R.drawable.ic_action_good_pushed);
        } else {
            likeButton.setImageResource(R.drawable.ic_action_good_white);
        }
        if (dislikePushed == 1) {
            dislikeButton.setImageResource(R.drawable.ic_action_bad_pushed);
        } else {
            dislikeButton.setImageResource(R.drawable.ic_action_bad_white);
        }
    }

    /**
     * Handles button visibility
     * @param b true if visible false if not visible
     */
    private void buttonVisibility(boolean b){
        likeButton.setEnabled(b);
        dislikeButton.setEnabled(b);
        likeButton.setImageResource(R.drawable.ic_action_good);
        dislikeButton.setImageResource(R.drawable.ic_action_bad);

    }

}
