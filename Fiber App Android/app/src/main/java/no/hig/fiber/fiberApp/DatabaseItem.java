package no.hig.fiber.fiberApp;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Database item
 * Handles insert, update and get item
 * against SQLite local database
 */
public class DatabaseItem {
    // Class data
    private long _id;
    private String itemId;
    private String type;                 // A, V or R
    public int pushedLike;              // 1 if pushed like else 0
    public int pushedDislike;           // 1 if pushed dislike, else 0
    // Table name
    private static final String LIKE_DISLIKE_TABLE_NAME = "likeDislike";
    // Column names:
    private static final String ID = "_id";
    private static final String ITEM_ID = "itemId";
    private static final String TYPE = "type";
    private static final String PUSHED_LIKE = "pushedLike";
    private static final String PUSHED_DISLIKE = "pushedDislike";
    // SQL statement to create table:
    public static final String LIKE_DISLIKE_CREATE_TABLE = "CREATE TABLE " + DatabaseItem.LIKE_DISLIKE_TABLE_NAME + " ("
            + DatabaseItem.ID + " INTEGER PRIMARY KEY ,"
            + DatabaseItem.ITEM_ID + " TEXT,"
            + DatabaseItem.TYPE + " TEXT ,"
            + DatabaseItem.PUSHED_LIKE + " INTEGER,"
            + DatabaseItem.PUSHED_DISLIKE + " INTEGER"
            + ");";

    public DatabaseItem() {}

    /**
     * Constructors that copy values into a DatabaseItem object
     * @param itemId Id
     * @param type type of medium
     * @param like pushed like or not
     * @param dislike pushed dislike or not
     */
    public DatabaseItem(String itemId, String type, int like, int dislike) {
        this.itemId = itemId;
        this.type = type;
        this.pushedLike = like;
        this.pushedDislike = dislike;
    }

    /**
     * Inserts the values in DatabaseItem object as a new row in local database
     * @param dbHelper dbHelper
     */
    public void save(DatabaseHelper dbHelper) {
        final ContentValues values = new ContentValues();
        values.put(ITEM_ID, this.itemId);
        values.put(TYPE, this.type);
        values.put(PUSHED_LIKE, this.pushedLike);
        values.put(PUSHED_DISLIKE, this.pushedDislike);
        final SQLiteDatabase db = dbHelper.getReadableDatabase();
        this._id = db.insert(LIKE_DISLIKE_TABLE_NAME, null, values);
        db.close();
    }

    /**
     * Gets all data on item from database that matches item id and type
     * @param dbHelper dbHelper
     * @param itemId id
     * @param type type of medium
     * @return item
     */
    public static DatabaseItem getItem(final DatabaseHelper dbHelper, String itemId, String type) {
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        String[] tableColumns = new String[]
                {ID, ITEM_ID, TYPE, PUSHED_LIKE, PUSHED_DISLIKE};
        String whereClause = ITEM_ID +" = ? AND " + TYPE + " = ?";
        String[] whereArgs = new String[] { itemId, type };
        final Cursor c = db.query(LIKE_DISLIKE_TABLE_NAME, tableColumns, whereClause,
                whereArgs, null, null, null);
        // Returns null if no item where found
        if(c.getCount() == 0) {
            return null;
        }
        c.moveToFirst();
        final DatabaseItem item = cursorToTheItem(c);
        c.close();
        db.close();
        return item;
    }

    /**
     * Sets all the information about the item
     * @param c cursor
     * @return item with data
     */
    private static DatabaseItem cursorToTheItem(Cursor c) {
        final DatabaseItem item = new DatabaseItem();
        item.setItem_id(c.getLong(c.getColumnIndex(ID)));
        item.setItemId(c.getString(c.getColumnIndex(ITEM_ID)));
        item.setType(c.getString(c.getColumnIndex(TYPE)));
        item.setPushedLike(c.getInt(c.getColumnIndex(PUSHED_LIKE)));
        item.setPushedDislike(c.getInt(c.getColumnIndex(PUSHED_DISLIKE)));
        return item;
    }

    /**
     * Updates if user has pushed like or dislike in database
     * Uses created DatabaseItem objects item id and type
     * @param dbHelper dbHelper
     */
    public void updateLikeDislike(DatabaseHelper dbHelper) {
        ContentValues values = new ContentValues();
        values.put(PUSHED_LIKE, this.pushedLike);
        values.put(PUSHED_DISLIKE, this.pushedDislike);
        String whereClause = ITEM_ID +" = ? AND " + TYPE + " = ?";
        String[] whereArgs = new String[] { this.itemId, this.type };
        final SQLiteDatabase db = dbHelper.getReadableDatabase();
        db.update(LIKE_DISLIKE_TABLE_NAME, values, whereClause, whereArgs);
        db.close();
    }

    /**
     * Sets database _id
     * @param _id database _id
     */
    void setItem_id(long _id) { this._id = _id; }

    /**
     * Sets item id
     * @param itemId item id
     */
    void setItemId(String itemId) { this.itemId = itemId; }

    /**
     * Sets type
     * @param type type of medium
     */
    void setType(String type) { this.type = type; }

    /**
     * Sets pushed like
     * @param pushedLike pushed like or not
     */
    void setPushedLike(int pushedLike) { this.pushedLike = pushedLike; }

    /**
     * Sets pushed dislike
     * @param pushedDislike pushed dislike or not
     */
    void setPushedDislike(int pushedDislike) { this.pushedDislike = pushedDislike; }
}
