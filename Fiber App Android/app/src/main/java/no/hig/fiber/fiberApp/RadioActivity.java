package no.hig.fiber.fiberApp;


import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;


public class RadioActivity extends FragmentActivity {

    AppFinalStrings strings = null;
    private TextView programTitleView;
    private TextView programDecrtiptionView;
    private TextView programAuthor;
    private TextView latestShowsText;
    private ImageView programImageView;
    private ImageButton playButton;
    private ListView programListView;
    private ImageButton logoButton;

    private int noOfShows = 5;
    private ArrayList <String> programList = new ArrayList<String>();
    private RadioActivityArrayAdapter adapter;

    private String ID;
    private int likes;
    private int dislikes;
    private String url;
    private String type = null;
    private Document XMLdoc= null;

    private View bckView;
    private ImageView bckImg;
    private ProgressBar bckProgress;


    /**
     * https://developer.chrome.com/multidevice/webview/gettingstarted
     * @param savedInstanceState saved instance
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        strings = new AppFinalStrings(this.getApplicationContext());
        type = strings.dbTypeRadio;


        setContentView(R.layout.radio_item_activity_layout);
        programTitleView = (TextView) findViewById(R.id.radio_program_title);
        programDecrtiptionView = (TextView) findViewById(R.id.radio_program_description);
        programAuthor = (TextView) findViewById(R.id.radio_program_author);
        programImageView = (ImageView) findViewById(R.id.radio_program_image);
        playButton = (ImageButton) findViewById(R.id.radio_program_playbutton);
        latestShowsText = (TextView) findViewById(R.id.radio_latest_show_text);
        bckView = findViewById(R.id.background);
        bckImg = (ImageView) findViewById(R.id.background_logo);
        bckProgress = (ProgressBar) findViewById(R.id.background_progressbar);

        latestShowsText.setVisibility(View.INVISIBLE);
        playButton.setVisibility(View.INVISIBLE);

        programListView = (ListView) findViewById(R.id.radio_program_list);
        adapter=new RadioActivityArrayAdapter(this, R.id.radio_program_list, programList);
        programListView.setAdapter(adapter);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
        }

         /*Setting title bar to show custom view*/
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.titlebar);

        ID = getIntent().getExtras().getString("itemID");
        url = getIntent().getExtras().getString("itemURL");
        this.likes = getIntent().getExtras().getInt("likes");
        this.dislikes = getIntent().getExtras().getInt("dislikes");

        // Sends ID and type to Fragment instance
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        LikeDislikeFragment fragmentLD = LikeDislikeFragment.newInstance(this.ID, this.type, this.likes, this.dislikes);
        fragmentTransaction.replace(R.id.like_dislike_fragment, fragmentLD);
        fragmentTransaction.commit();

        playButton.setOnClickListener(openPodcast);

        new FetchData().execute(url);

        logoButton = (ImageButton)findViewById(R.id.logo_button);
        logoButton.setOnClickListener(logo);
    }

    private View.OnClickListener openPodcast = new View.OnClickListener() {
        @Override
        public void onClick(View view){
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }
    };

    private class FetchData extends AsyncTask<String, Void, Document> {
        /**
         * Showing progress background while getting data
         */
        @Override
        protected void onPreExecute() {
            bckProgress.setVisibility(View.VISIBLE);
            bckImg.setVisibility(View.VISIBLE);
            bckView.setVisibility(View.VISIBLE);
            super.onPreExecute();
        }


        @Override
        protected Document doInBackground(String... url) {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db;
            if( XMLdoc == null) {
                try {
                    db = dbf.newDocumentBuilder();
                    XMLdoc = db.parse(url[0]);
                } catch (ParserConfigurationException e) {
                    Log.d(strings.exceptionTagInLog, e.getMessage() );
                } catch (SAXException e) {
                    Log.d(strings.exceptionTagInLog, e.getMessage());
                } catch (IOException e) {
                    Log.d(strings.exceptionTagInLog, e.getMessage());
                }
            }
            return XMLdoc;
        }



        @SuppressWarnings("unused")
        @Override
        protected void onPostExecute(Document XMLdoc) {
            if(programList.isEmpty()) {

                XMLdoc.getDocumentElement().normalize();

                programTitleView.setText(XMLdoc.getElementsByTagName("title").item(0).getTextContent());

                String tmpImageURL = XMLdoc.getElementsByTagName("itunes:image").item(0).getAttributes().getNamedItem("href").getTextContent();
                Log.d("TEST imageurl: ", tmpImageURL);
                try {
                    URL ImageURL = new URL(tmpImageURL);
                    new DownloadImageTask().execute(ImageURL);
                } catch (MalformedURLException e) {
                    Log.d(strings.exceptionTagInLog, e.getMessage());
                }
                programDecrtiptionView.setText(XMLdoc.getElementsByTagName("description").item(0).getTextContent());
                programAuthor.setText(XMLdoc.getElementsByTagName("itunes:author").item(0).getTextContent());


                NodeList itemNodes = XMLdoc.getElementsByTagName("item");  //---retrieve all the <item> nodes---
                if(itemNodes.getLength() < noOfShows){
                    noOfShows = itemNodes.getLength();
                }
                for (int i = 0; i < noOfShows; i++) {    // Go through all the nodes to build items
                    Node itemNode = itemNodes.item(i);
                    if (itemNode.getNodeType() == Node.ELEMENT_NODE) {

                        //---convert the Node into an Element---
                        Element itemElement = (Element) itemNode;

                        String tmpEpisodeTitle = itemElement.getElementsByTagName("title").item(0).getTextContent();

                        //String[] date = itemElement.getElementsByTagName("pubDate").item(0).getTextContent().split(" ");
                        //String tmpDate = date[1] + " " + date[2] + " " + date[3];
                        programList.add(tmpEpisodeTitle);
                    }
                }
                if(!programList.isEmpty()){
                    latestShowsText.setVisibility(View.VISIBLE);
                }
                bckView.setVisibility(View.INVISIBLE);
                bckImg.setVisibility(View.INVISIBLE);
                bckProgress.setVisibility(View.INVISIBLE);
                playButton.setVisibility(View.VISIBLE);
            }
        }
    }

    private class DownloadImageTask extends AsyncTask<URL, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(URL... urls) {
            Bitmap bitmap = null;
            InputStream in;
            int response;

            try {
                final URLConnection conn = urls[0].openConnection();

                if (conn instanceof HttpURLConnection) {
                    // Setup connection
                    final HttpURLConnection httpConn = (HttpURLConnection) conn;
                    httpConn.setAllowUserInteraction(false);
                    httpConn.setInstanceFollowRedirects(true);
                    httpConn.setRequestMethod("GET");
                    httpConn.connect();

                    // Test connection
                    response = httpConn.getResponseCode();
                    // If response OK - Download image
                    if (response == HttpURLConnection.HTTP_OK) {
                        in = httpConn.getInputStream();
                        bitmap = BitmapFactory.decodeStream(in);
                        in.close();
                    }
                }
            } catch (IOException e) {
                Log.d(strings.exceptionTagInLog, e.getMessage());
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            programImageView.setImageBitmap(bitmap);
        }
    }

    private View.OnClickListener logo = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };
}