package no.hig.fiber.fiberApp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.util.ByteArrayBuffer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

@SuppressWarnings("EmptyMethod")
public class TabArticles extends Fragment {
    AppFinalStrings strings = null;
    private String rssFeedUrl = null;
    private ListView articleListViewSorted;
    private ListView articleListViewRanked;
    private ArticleArrayAdapter sortedAdapter = null;
    private ArticleArrayAdapter rankedAdapter = null;
    private ArrayList<ArticleItem> articleList = new ArrayList<ArticleItem>();
    private ArrayList<ArticleItem> articleSortedList = new ArrayList<ArticleItem>();
    private ArrayList<ArticleItem> articleRankedList = new ArrayList<ArticleItem>();
    private int noInShowingList = 5;
    private String rssFeed = null;
    private View avis = null;
    private boolean forceUpdate = false;
    private boolean showRanked = false;
    private OnFirstArticleListener firstArticleListener;
    private ImageButton fiberHome;
    private Button latestButton;
    private Button popularButton;
    private View bckView;
    private ImageView bckImg;
    private ProgressBar bckProgress;
    private Context appContext;


    /*
     * To update to ranked run:
     *  showRanked = true;
     *  choseList();
     */


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        appContext = this.getActivity().getApplicationContext();
        strings = new AppFinalStrings(appContext);
        rssFeedUrl = strings.fiberWordpressRssUrl;
        noInShowingList = strings.noOfItemsInTabLists;

        avis = inflater.inflate(R.layout.tab_article_fragmentlayout, container, false);


        articleListViewRanked = (ListView) avis.findViewById(R.id.article_list_view_ranked);
        rankedAdapter = new ArticleArrayAdapter(avis.getContext(), R.id.article_list_view_ranked, articleRankedList);
        articleListViewRanked.setAdapter(rankedAdapter);
        articleListViewRanked.setOnItemClickListener(item);

        articleListViewSorted = (ListView) avis.findViewById(R.id.article_list_view_sorted);
        sortedAdapter = new ArticleArrayAdapter(avis.getContext(), R.id.article_list_view_sorted, articleSortedList);
        articleListViewSorted.setAdapter(sortedAdapter);
        articleListViewSorted.setOnItemClickListener(item);

        bckView =  avis.findViewById(R.id.background);
        bckImg = (ImageView) avis.findViewById(R.id.background_logo);
        bckProgress = (ProgressBar) avis.findViewById(R.id.background_progressbar);

        fiberHome = (ImageButton) avis.findViewById(R.id.refresh_bottom);
        latestButton = (Button) avis.findViewById(R.id.latest_button);
        popularButton = (Button) avis.findViewById(R.id.popular_button);

        fiberHome.setOnClickListener(fiber);
        latestButton.setOnClickListener(latest);
        popularButton.setOnClickListener(popular);

        if(articleList.isEmpty()) {
            new BuildArticleList().execute(rssFeedUrl);
        }else {
            chooseList();
        }

        fiberHome.setOnLongClickListener(new View.OnLongClickListener()  {
            @Override
            public boolean onLongClick(View view) {
                strings.forceRemoteConfigUpdate();
                return true;
            }
        });

        return avis;
    }



    /**
     * Gets likes and dislikes in case of updates
     */
    @Override
    public void onResume(){
        super.onResume();
        if(!(articleSortedList.isEmpty())){
            getLikesDislikes();
        }
    }

    /**
     * Interface for sending the newest article id to the connected Activity
     */
    public interface OnFirstArticleListener {
        public void getFirstArticleId(String firstId);
    }

    /**
     * Checks that the container activity has the interface
     * http://developer.android.com/training/basics/fragments/communicating.html
     * @param activity the connected activity
     */
    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        if (activity instanceof OnFirstArticleListener) {
            firstArticleListener = (OnFirstArticleListener) activity;
        }


    }
    /* -------- ONCLICK LISTENERS -------- */

    /**
     * Anonymous function for onclick listener for the fiber button
     */
    private View.OnClickListener fiber = new View.OnClickListener()  {
        /**
         * Sets buttons and sorts list after latest items
         * @param view view
         */
        @Override
        public void onClick(View view){
           forceUpdate = true;
           showRanked = false;
            new BuildArticleList().execute(rssFeedUrl);

        }
    };

    /**
     * Anonymous function for onclick listener for the latest button
     */
    private View.OnClickListener latest = new View.OnClickListener()  {
        /**
         * Sets buttons and sorts list after latest items
         * @param view view
         */
        @Override
        public void onClick(View view){
            latestButton.setBackground(getResources().getDrawable(R.drawable.button_active));
            latestButton.setTextColor(getResources().getColor(R.color.dark_grey));
            popularButton.setBackgroundColor(Color.TRANSPARENT);
            popularButton.setTextColor(getResources().getColor(R.color.white));
            showRanked = false;
            chooseList();
        }
    };

    /**
     * Anonymous function for onclick listener for the popular button
     */
    private View.OnClickListener popular = new View.OnClickListener() {
        /**
         * Sets buttons and sorts list after most popular items
         * @param view view
         */
        @Override
        public void onClick(View view){
            popularButton.setBackground(getResources().getDrawable(R.drawable.button_active));
            popularButton.setTextColor(getResources().getColor(R.color.dark_grey));
            latestButton.setBackgroundColor(Color.TRANSPARENT);
            latestButton.setTextColor(getResources().getColor(R.color.white));
            showRanked = true;
            chooseList();
        }
    };

    private void UpdateRankedList() {
        articleRankedList.clear();
        for(int i = 0; i < articleList.size(); i++) {
            articleRankedList.add(articleList.get(i));
        }
        Collections.sort(articleRankedList, new customComparator());

        // If more than it is supposed to in the list, remove the latest
        while (articleRankedList.size() > noInShowingList) {
            articleRankedList.remove(noInShowingList);
        }

        for(int i = 0; i < articleRankedList.size(); i++){
            articleRankedList.get(i).StartImageDownloading();
        }

        rankedAdapter.notifyDataSetChanged();
    }

    private void chooseList(){
        bckView.setVisibility(View.INVISIBLE);
        bckImg.setVisibility(View.INVISIBLE);
        bckProgress.setVisibility(View.INVISIBLE);
        articleListViewSorted.setVisibility(View.INVISIBLE);
        articleListViewRanked.setVisibility(View.INVISIBLE);
        if(showRanked) {
            UpdateRankedList();
            articleListViewRanked.setVisibility(View.VISIBLE);
        }else{
            articleListViewSorted.setVisibility(View.VISIBLE);
        }


    }



    /**
     * Task, responsible for downloading a RSS feed a given URL.
     * This task will execute the download asynchronously, and fill up the
     * UI field when ready.
     */

    private class BuildArticleList extends AsyncTask<String, Integer, String> {
        /**
         * Showing progress background while getting data
         */
        @Override
        protected void onPreExecute() {
            bckProgress.setVisibility(View.VISIBLE);
            bckImg.setVisibility(View.VISIBLE);
            bckView.setVisibility(View.VISIBLE);
            articleListViewRanked.setVisibility(View.INVISIBLE);
            articleListViewSorted.setVisibility(View.INVISIBLE);
            super.onPreExecute();
        }

        /**
         * We wanted it to get the raw xml file with all data.
         * http://stackoverflow.com/questions/13832039/download-xml-to-string
         *
         * @param url string URL
         * @return xml string structure
         */
        @Override
        protected String doInBackground(String... url) {
            if( rssFeed == null || forceUpdate) {
                URL tmpUrl = null;
                try {
                    tmpUrl = new URL(url[0]);
                } catch (MalformedURLException e) {
                    Log.d(strings.exceptionTagInLog, e.getMessage());
                }
                try {
                    URLConnection urlCon;
                    if (tmpUrl != null) {
                        urlCon = tmpUrl.openConnection();

                        InputStream is = urlCon.getInputStream();
                        BufferedInputStream bis = new BufferedInputStream(is);

                        ByteArrayBuffer baf = new ByteArrayBuffer(262144);      // 256 KB
                        int current;
                        while ((current = bis.read()) != -1) {
                            baf.append((byte) current);
                        }
                        rssFeed = new String(baf.toByteArray(), "UTF8");
                    }
                } catch (UnsupportedEncodingException e) {
                    Log.d(strings.exceptionTagInLog, e.getMessage());
                } catch (ClientProtocolException e) {
                    Log.d(strings.exceptionTagInLog, e.getMessage());
                } catch (IOException e) {
                    Log.d(strings.exceptionTagInLog, e.getMessage());
                }
                // return XML
            }
            return rssFeed;
        }


        @Override
        protected void onPostExecute(String RSS) {
            if(articleSortedList.isEmpty() | forceUpdate) {
                articleSortedList.clear();
                articleList.clear();
                Document doc = null;
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db;
                InputSource is = new InputSource();

                try {
                    db = dbf.newDocumentBuilder();
                    is.setCharacterStream(new StringReader(RSS));
                    if (is != null) {
                        doc = db.parse(is);
                    }
                }
                  catch (NullPointerException e){
                      Toast.makeText(avis.getContext(), getString(R.string.no_internet_connection), Toast.LENGTH_LONG).show();
                    Log.d(strings.exceptionTagInLog, "No Internet Connection");  // No internet connection to RSS
                } catch (ParserConfigurationException e) {
                    Log.d(strings.exceptionTagInLog, e.getMessage() );
                } catch (SAXException e) {
                    Log.d(strings.exceptionTagInLog, e.getMessage() );
                } catch (IOException e) {
                    Log.d(strings.exceptionTagInLog, e.getMessage());
                }

                if (doc != null) {
                    doc.getDocumentElement().normalize();

                    NodeList itemNodes = doc.getElementsByTagName("item");  //---retrieve all the <item> nodes---

                    int sortListCount = 0;
                    for (int i = 0; i < itemNodes.getLength(); i++) {    // Go through all the nodes to build items
                        Node itemNode = itemNodes.item(i);
                        if (itemNode.getNodeType() == Node.ELEMENT_NODE) {
                            String tmpImageURL = "null";

                            //---convert the Node into an Element---
                            Element itemElement = (Element) itemNode;

                            String tmpTitle = itemElement.getElementsByTagName("title").item(0).getTextContent();


                            String tmpID = itemElement.getElementsByTagName("guid").item(0).getTextContent();
                            tmpID = tmpID.split("=")[1];

                            String tmpURL = itemElement.getElementsByTagName("link").item(0).getTextContent();

                            String tmpDate = itemElement.getElementsByTagName("pubDate").item(0).getTextContent();
                            String[] date = tmpDate.split(" ");
                            tmpDate = date[1] + " " + date[2] + " " + date[3];


                            String description = itemElement.getElementsByTagName("description").item(0).getTextContent();
                            DocumentBuilderFactory dbFactoryCDATA = DocumentBuilderFactory.newInstance();
                            DocumentBuilder dBuilderCDATA;
                            try {
                                dBuilderCDATA = dbFactoryCDATA.newDocumentBuilder();
                                Document docCDATA = dBuilderCDATA.parse(new ByteArrayInputStream(("<root>" + description + "</root>").getBytes()));
                                docCDATA.getDocumentElement().normalize();

                                NodeList alist = docCDATA.getElementsByTagName("img");

                                if (alist.getLength() != 0) {
                                    Node anode = alist.item(0);
                                    tmpImageURL = anode.getAttributes().getNamedItem("src").getNodeValue();
                                }

                            } catch (ParserConfigurationException e) {
                                Log.d(strings.exceptionTagInLog, e.getMessage() );
                            } catch (SAXException e) {
                                Log.d(strings.exceptionTagInLog, e.getMessage() );
                            } catch (IOException e) {
                                Log.d(strings.exceptionTagInLog, e.getMessage() );
                            }

                            ArticleItem tmp = new ArticleItem(tmpID, tmpTitle, tmpURL, tmpDate, tmpImageURL, sortedAdapter, rankedAdapter, appContext);
                            //Add the new item to the list
                            articleList.add(tmp);


                            NodeList categoryNodes = itemElement.getElementsByTagName("category");
                            boolean toSort = false;
                            for (int j = 0; j < categoryNodes.getLength(); j++) {    // Go through all the nodes to build items
                                String category = itemElement.getElementsByTagName("category").item(0).getTextContent();
                                if(category.equals("Forside"))
                                    toSort = true;
                            }
                            if(toSort && sortListCount < noInShowingList) {
                                sortListCount++;
                                articleSortedList.add(tmp);
                                tmp.StartImageDownloading();
                            }
                        }
                    }
                }
            }

            chooseList();
            forceUpdate = false;
            // Calls listeners function so Activity can get the newest article id
            if(!articleSortedList.isEmpty()) {
                firstArticleListener.getFirstArticleId((articleSortedList.get(0).ID));
            }
        }
    }


    /**
     * Gets like and dislike data for each item in article list
     */
    private void getLikesDislikes() {
        for(ArticleItem item : articleList) {
            item.new GetArticleLikeDislike().execute();
        }
    }

    private AdapterView.OnItemClickListener item = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> adapterView, final View view, int position, long itemId) {
            ArticleItem item;
            if(showRanked){
               item = articleRankedList.get(position);
            }else {
               item = articleSortedList.get(position);
            }
            Intent intent = new Intent(adapterView.getContext(), ArticleActivity.class);
            intent.putExtra("itemID", item.ID);
            intent.putExtra("itemURL", item.url);
            intent.putExtra("likes", item.likes);
            intent.putExtra("dislikes", item.dislikes);
            startActivity(intent);
        }
    };


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);


    }

    /**
     * Comparator principle taken from the following code snippet.
     * http://stackoverflow.com/questions/2784514/sort-arraylist-of-custom-objects-by-property
     */
    private class customComparator implements Comparator<ArticleItem> {
        public int compare(ArticleItem ai1, ArticleItem ai2) {
            return ((ai2.likes+ai2.dislikes)-(ai1.likes+ai1.dislikes));
        }
    }

}
