package no.hig.fiber.fiberApp;


import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.android.youtube.player.YouTubePlayer.Provider;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class VideoActivity extends YouTubeBaseActivity implements
        YouTubePlayer.OnInitializedListener{

    private AppFinalStrings strings = null;
    private String API_KEY = null;
    private String type = null;

    private String ID;
    private String title;
    private String description;
    private String date;
    private TextView programTitleView;
    private TextView programDescriptionView;
    private TextView programDateView;
    private int likes;
    private int dislikes;
    private ImageButton logoButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        strings = new AppFinalStrings(this.getApplicationContext());
        API_KEY = strings.googleFiber_API_KEY;
        type = strings.dbTypeVideo;



        setContentView(R.layout.video_interface_layout);

        ID = getIntent().getExtras().getString("itemID");
        title = getIntent().getExtras().getString("itemTitle");
        description = getIntent().getExtras().getString("itemDescription");
        date = getIntent().getExtras().getString("itemDate");
        this.likes = getIntent().getExtras().getInt("likes");
        this.dislikes = getIntent().getExtras().getInt("dislikes");

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
        }

         /*Setting title bar to show custom view*/
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.titlebar);

            // Sends ID and type to Fragment instance
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        LikeDislikeFragment fragmentLD = LikeDislikeFragment.newInstance(this.ID, this.type, this.likes, this.dislikes);
        ft.replace(R.id.like_dislike_fragment, fragmentLD);
        ft.commit();

        YouTubePlayerView youTubePlayerView = (YouTubePlayerView)findViewById(R.id.youtube_player_view);
        youTubePlayerView.initialize(API_KEY, this);

        programTitleView = (TextView) findViewById(R.id.video_title);
        programDescriptionView = (TextView) findViewById(R.id.video_description);
        programDateView = (TextView) findViewById(R.id.video_date);
        programTitleView.setText(title);
        programDescriptionView.setText(description);
        programDateView.setText(date);

        logoButton = (ImageButton)findViewById(R.id.logo_button);
        logoButton.setOnClickListener(logo);
    }

    @Override
    public void onInitializationFailure(Provider provider,
                                        YouTubeInitializationResult result) {
        Toast.makeText(getApplicationContext(),
                "onInitializationFailure()",
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onInitializationSuccess(Provider provider, YouTubePlayer player,
                                        boolean wasRestored) {
        if (!wasRestored) {
            player.cueVideo(ID);
        }
    }

    private View.OnClickListener logo = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };
}