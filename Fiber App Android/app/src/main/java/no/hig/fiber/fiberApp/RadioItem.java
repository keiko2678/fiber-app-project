package no.hig.fiber.fiberApp;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

class RadioItem {
    AppFinalStrings strings = null;
    public String ID;
    public String title;
    public String url;
    private String type = null;
    public int likes = 0;
    public int dislikes = 0;
    public String date;
    private int index;
    public Bitmap picture = null;
    private URL imageURL;
    private boolean gotLikeDislike= false;
    private RadioArrayAdapter sortedAdapter;
    private RadioArrayAdapter rankedAdapter;
    private ApiHandler api;
    private boolean imageDownloading = false;


    // Empty construtor
    public RadioItem(){}

    /**
     * Constructor
     * @param ID Radio ID
     * @param title Radio title
     * @param url Radio url
     * @param date Radio date published
     * @param imgURL Radio Thumbnail Image url
     * @param sortedAdapter Sorted adapter for notify
     * @param rankedAdapter Sorted adapter for notify
     */
    public RadioItem(int index, String ID, String title, String url, String date, String imgURL,
                     RadioArrayAdapter sortedAdapter, RadioArrayAdapter rankedAdapter, Context context) {
        strings = new AppFinalStrings(context);
        type = strings.dbTypeRadio;

        this.index = index;
        this.ID = ID;
        this.title = title;
        this.url = url;
        this.date = date;
        this.sortedAdapter = sortedAdapter;
        this.rankedAdapter = rankedAdapter;

        api = new ApiHandler(this.ID, this.type, context);
        if (!gotLikeDislike) {
            new GetRadioLikeDislike().execute();
        }

        try {
            this.imageURL = new URL(imgURL);
        } catch (MalformedURLException e) {
            Log.d(strings.exceptionTagInLog, e.getMessage() );
        }

    }

    public void StartImageDownloading() {
        if (!imageDownloading) {
            imageDownloading = true;
            if (imageURL != null) {
                new DownloadImageTask().execute(imageURL);
            }
        }
    }

    private class DownloadImageTask extends AsyncTask<URL, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(URL... urls) {
            Bitmap bitmap = null;
            InputStream in;
            int response;

            try {
                final URLConnection conn = urls[0].openConnection();

                if (conn instanceof HttpURLConnection) {
                    // Setup connection
                    final HttpURLConnection httpConn = (HttpURLConnection) conn;
                    httpConn.setAllowUserInteraction(false);
                    httpConn.setInstanceFollowRedirects(true);
                    httpConn.setRequestMethod("GET");
                    httpConn.connect();

                    // Test connection
                    response = httpConn.getResponseCode();
                    // If response OK - Download image
                    if (response == HttpURLConnection.HTTP_OK) {
                        in = httpConn.getInputStream();
                        bitmap = BitmapFactory.decodeStream(in);
                        in.close();
                    }
                }
            } catch (IOException e) {
                Log.d(strings.exceptionTagInLog, e.getMessage() );
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            picture = bitmap;
            sortedAdapter.notifyDataSetChanged();
            rankedAdapter.notifyDataSetChanged();
        }
    }
    /**
     * AsyncTask for handling getting like and dislike data from API
     */
    public class GetRadioLikeDislike extends AsyncTask<Void,  Void, ApiHandler>{
        /**
         * Gets data from API via ApiHandler
         * @return api object with the new data
         */
        @Override
        protected ApiHandler doInBackground(Void... params) {
            api.getLikeDislike();
            return api;
        }
        /**
         * Updates choosenAdapter with new like and dislike data
         * @param api with new data
         */
        protected void onPostExecute(ApiHandler api) {
            likes = api.likes;
            dislikes = api.dislikes;
            gotLikeDislike=true;
            sortedAdapter.notifyDataSetChanged();
            rankedAdapter.notifyDataSetChanged();
        }
    }
}