package no.hig.fiber.fiberApp;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.util.ByteArrayBuffer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import static java.util.concurrent.TimeUnit.*;

/**
 * In order to have all final strings at one place
 * Created by pckofstad on 06.12.14.
 * Updated with remoteconfig july 2015
 *
 */
public class AppFinalStrings extends Activity {

    private final String youtubeFiberTVplaylistID = "UUa0XHGDbBL8re8UgO-OWNPA";
    public final String googleFiber_API_KEY = "AIzaSyBZ2VFLWbllFKKJMFE0UPsnjgGHqOPUMDs";    // Updated to the fiber-app-project 30. march 2015
    public final String youtubeFiberTVJsonListUrl ="https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId="
            +youtubeFiberTVplaylistID
            +"&key="+googleFiber_API_KEY
            +"&maxResults=50";

    public final int apiVersionNumber = 1;
    public final int configVersionNumber = 1;
    public final String dbTypeVideo = "V";
    public final String dbTypeArticle = "A";
    public final String dbTypeRadio = "P";
    public final String exceptionTagInLog = "--- Warning ---";
    public final String infoTagInLog = "--- INFO ---";
    public final int noOfItemsInTabLists = 20;
    private final String dateTimeFormat = "yyyy:MM:dd-HH:mm:ss";

    private String rssFeed = null;
    private boolean Updating = false;
    private Context appContext;

    // Default string values
    private static final String defaultfiberRadioPodcastRssUrl = "http://fiber.hig.no/fiberapp/radio/fiber-radio-podcasts.xml";
    private static final String defaultfiberWordpressRssUrl = "http://fiber.hig.no/?feed=rss";
    private static final String defaultfiberApiUrl = "http://keikoinnovation.duckdns.org:7171";
    private static final String defaultfiberMessagesRssUrl = "http://fiber.hig.no/fiberapp/fiber-remote-message.xml";
    private static final String defaultfiberRemoteConfigRssUrl = "http://fiber.hig.no/fiberapp/fiber-remote-config.xml";
    private static final int defaultUpdateIntevalDays = 20;

    public static final String PREFS_NAME = "FiberAppPrefsFile";

    // Strings that could be changed by remote config
    String fiberRadioPodcastRssUrl          = defaultfiberRadioPodcastRssUrl;
    String fiberWordpressRssUrl             = defaultfiberWordpressRssUrl;
    String fiberApiUrl                      = defaultfiberApiUrl;
    private String fiberRemoteConfigRssUrl  = defaultfiberRemoteConfigRssUrl;
    String fiberMessagesRssUrl              = defaultfiberMessagesRssUrl;
    int updateIntervalDays                  = defaultUpdateIntevalDays;
    Date lastUpdateTime                     = null;

    /**
     * Constructor that aims to check when the strings was last updated and do a remoteConfig check if suited.
     */
    public AppFinalStrings(Context context) {
        appContext = context;
        readFinalStrings();
    }

    public void forceRemoteConfigUpdate(){
        Updating = true;
        new getRemoteConfig().execute(fiberRemoteConfigRssUrl);
    }

    public void remoteConfigUpdate(){
        Log.d(infoTagInLog, "Cheking if RemoteConfig Update");
        if(lastUpdateTime != null ) {
            Date nowTime = Calendar.getInstance().getTime();
            long MIN_INTERVAL = MILLISECONDS.convert(updateIntervalDays, DAYS);
            long checkInterval = nowTime.getTime() - lastUpdateTime.getTime();

            if (checkInterval >= MIN_INTERVAL ) {
                if(!Updating) {
                    Updating =true;
                    new getRemoteConfig().execute(fiberRemoteConfigRssUrl);
                }
            } else
                Log.d(infoTagInLog, "To soon for remote update");
        } else {
            new getRemoteConfig().execute(fiberRemoteConfigRssUrl);
        }

    }

    private void saveFinalStrings(){
        try{
            SharedPreferences preferences = appContext.getSharedPreferences(PREFS_NAME, appContext.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();

            editor.putString("fiberRadioPodcastRssUrl", fiberRadioPodcastRssUrl);
            editor.putString("fiberWordpressRssUrl", fiberWordpressRssUrl);
            editor.putString("fiberApiUrl", fiberApiUrl);
            editor.putString("fiberRemoteConfigRssUrl", fiberRemoteConfigRssUrl);
            editor.putString("fiberMessagesRssUrl", fiberMessagesRssUrl);
            editor.putInt("updateIntervalDays", defaultUpdateIntevalDays);
            editor.putString("lastUpdateTime", dateToString(lastUpdateTime));

            editor.commit();
            Log.d(infoTagInLog, "RemoteConfig written to " + PREFS_NAME + " !");
        } catch(NullPointerException ex){
            Log.d(exceptionTagInLog, "Failded to write " + PREFS_NAME + ": " + ex);
        }

    }

    private void readFinalStrings() {
        try{
            SharedPreferences preferences = appContext.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

            fiberRadioPodcastRssUrl = preferences.getString("fiberRadioPodcastRssUrl", defaultfiberRadioPodcastRssUrl);
            fiberWordpressRssUrl = preferences.getString("fiberWordpressRssUrl",defaultfiberWordpressRssUrl);
            fiberApiUrl = preferences.getString("fiberApiUrl", defaultfiberApiUrl);
            fiberRemoteConfigRssUrl = preferences.getString("fiberRemoteConfigRssUrl", defaultfiberRemoteConfigRssUrl);
            fiberMessagesRssUrl = preferences.getString("fiberMessagesRssUrl", defaultfiberMessagesRssUrl);
            updateIntervalDays  = preferences.getInt("updateIntervalDays", defaultUpdateIntevalDays);
            lastUpdateTime = stringToDate(preferences.getString("lastUpdateTime", null));
        } catch(NullPointerException ex){
            Log.d(exceptionTagInLog, "Failded to read " + PREFS_NAME + ": " + ex);
        }


    }

    /*
     * To be futher built later.
     * Tested and found working in eclipse.
     */
    private Date stringToDate(String tmp){
        Date rtnTime=null;
        try {
            rtnTime = new SimpleDateFormat("yyyy:MM:dd-HH:mm:ss").parse(tmp);
        } catch (ParseException e) {
            Log.d(exceptionTagInLog, e.getMessage() );
        }
        return rtnTime;
    }

    private String dateToString (Date tmp){
        String rtnTime = new SimpleDateFormat("yyyy:MM:dd-HH:mm:ss").format(tmp);
        return rtnTime;
    }


    /**
     * Task, responsible for downloading a RSS feed a given URL.
     * This task will execute the download asynchronously.
     */

    private class getRemoteConfig extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * @param url string URL
         * @return xml string structure
         */
        @Override
        protected String doInBackground(String... url) {
            if( rssFeed == null || Updating) {
                URL tmpUrl = null;
                try {
                    tmpUrl = new URL(url[0]);
                } catch (MalformedURLException e) {
                    Log.d(exceptionTagInLog, e.getMessage());
                }
                try {
                    URLConnection urlCon;
                    if (tmpUrl != null) {
                        urlCon = tmpUrl.openConnection();

                        InputStream is = urlCon.getInputStream();
                        BufferedInputStream bis = new BufferedInputStream(is);

                        ByteArrayBuffer baf = new ByteArrayBuffer(262144);      // 256 KB
                        int current;
                        while ((current = bis.read()) != -1) {
                            baf.append((byte) current);
                        }
                        rssFeed = new String(baf.toByteArray(), "UTF8");
                    }
                } catch (UnsupportedEncodingException e) {
                    Log.d(exceptionTagInLog, e.getMessage());
                } catch (ClientProtocolException e) {
                    Log.d(exceptionTagInLog, e.getMessage());
                } catch (IOException e) {
                    Log.d(exceptionTagInLog, e.getMessage());
                }
                // return XML
            }
            return rssFeed;
        }


        @Override
        protected void onPostExecute(String RSS) {
            if( lastUpdateTime == null || Updating) {
                Document doc = null;
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db;
                InputSource is = new InputSource();

                try {
                    db = dbf.newDocumentBuilder();
                    is.setCharacterStream(new StringReader(RSS));
                    if (is != null) {
                        doc = db.parse(is);
                    }
                }
                catch (NullPointerException e){
                    Log.d(exceptionTagInLog, "No Internet Connection");  // No internet connection to RSS
                } catch (ParserConfigurationException e) {
                    Log.d(exceptionTagInLog, e.getMessage() );
                } catch (SAXException e) {
                    Log.d(exceptionTagInLog, e.getMessage() );
                } catch (IOException e) {
                    Log.d(exceptionTagInLog, e.getMessage());
                }

                if (doc != null) {
                    doc.getDocumentElement().normalize();

                    Node itemNode = doc.getElementsByTagName("app").item(0);

                    if (itemNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element configElements = (Element) itemNode;

                        int tmpConfigVersionNumber = Integer.parseInt(configElements.getElementsByTagName("configversionnumber").item(0).getTextContent());
                        String tmpAppName = configElements.getElementsByTagName("title").item(0).getTextContent();
                        if (configVersionNumber == tmpConfigVersionNumber && tmpAppName.equals("Fiber")) {

                            // Retrieve config information
                            String fiberRadioPodcastRssUrl = configElements.getElementsByTagName("fiberRadioPodcastRssUrl").item(0).getTextContent();
                            String fiberWordpressRssUrl = configElements.getElementsByTagName("fiberWordpressRssUrl").item(0).getTextContent();
                            String fiberApiUrl = configElements.getElementsByTagName("fiberApiUrl").item(0).getTextContent();
                            String fiberRemoteConfigRssUrl = configElements.getElementsByTagName("remoteConfigUrl").item(0).getTextContent();
                            String fiberMessagesRssUrl = configElements.getElementsByTagName("remoteMessagesUrl").item(0).getTextContent();
                            int updateIntervalDays = Integer.parseInt(configElements.getElementsByTagName("updateIntervalDays").item(0).getTextContent());

                            Log.d(infoTagInLog, "RemoteConfig Updated: fiberRadioPodcastRssUrl: " + fiberRadioPodcastRssUrl);
                            Log.d(infoTagInLog, "RemoteConfig Updated: fiberWordpressRssUrl: " + fiberWordpressRssUrl);
                            Log.d(infoTagInLog, "RemoteConfig Updated: fiberApiUrl: " + fiberApiUrl);
                            Log.d(infoTagInLog, "RemoteConfig Updated: fiberRemoteConfigRssUrl: " + fiberRemoteConfigRssUrl);
                            Log.d(infoTagInLog, "RemoteConfig Updated: fiberMessagesRssUrl: " + fiberMessagesRssUrl);
                            Log.d(infoTagInLog, "RemoteConfig Updated: updateIntervalDays: " + updateIntervalDays);

                            // Check valid values
                            if(fiberRadioPodcastRssUrl == null){
                                fiberRadioPodcastRssUrl = defaultfiberRadioPodcastRssUrl;
                                Log.d(exceptionTagInLog, "RemoteConfig not found, changed to default value: defaultfiberRadioPodcastRssUrl: " + defaultfiberRadioPodcastRssUrl);
                            }
                            if(fiberWordpressRssUrl == null){
                                fiberWordpressRssUrl = defaultfiberWordpressRssUrl;
                                Log.d(exceptionTagInLog, "RemoteConfig not found, changed to default value: defaultfiberWordpressRssUrl: " + defaultfiberWordpressRssUrl);
                            }
                            if(fiberApiUrl == null) {
                                fiberApiUrl = defaultfiberApiUrl;
                                Log.d(exceptionTagInLog, "RemoteConfig not found, changed to default value: defaultfiberApiUrl: " + defaultfiberApiUrl);
                            }
                            if(fiberRemoteConfigRssUrl == null){
                                fiberRemoteConfigRssUrl = defaultfiberRemoteConfigRssUrl;
                                Log.d(exceptionTagInLog, "RemoteConfig not found, changed to default value: defaultfiberRemoteConfigRssUrl: " + defaultfiberRemoteConfigRssUrl);
                            }
                            if(fiberMessagesRssUrl == null){
                                fiberMessagesRssUrl = defaultfiberMessagesRssUrl;
                                Log.d(exceptionTagInLog, "RemoteConfig not found, changed to default value: defaultfiberMessagesRssUrl: " + defaultfiberMessagesRssUrl);
                            }
                            if( updateIntervalDays == 0 ){
                                updateIntervalDays = defaultUpdateIntevalDays;
                                Log.d(exceptionTagInLog, "RemoteConfig not found, changed to default value: defaultUpdateIntevalDays: " + defaultUpdateIntevalDays);
                            }

                            lastUpdateTime = Calendar.getInstance().getTime();
                            Log.d(infoTagInLog, "RemoteConfig Updated: lastUpdateTime: " + lastUpdateTime);

                            saveFinalStrings();
                        }
                    }
                }
            }
            Updating = false;

        }
    }


}

