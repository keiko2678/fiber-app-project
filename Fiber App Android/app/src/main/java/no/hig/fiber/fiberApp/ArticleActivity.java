package no.hig.fiber.fiberApp;

import android.app.ActionBar;;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;


public class ArticleActivity extends FragmentActivity {
    WebView webView;
    private View bckView;
    private ImageView bckImg;
    private ProgressBar bckProgress;
    private ImageButton logoButton;
    /**
     * https://developer.chrome.com/multidevice/webview/gettingstarted
     *
     * @param savedInstanceState saved instance
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.article_item_activity_layout);

        bckView = findViewById(R.id.background);
        bckImg = (ImageView) findViewById(R.id.background_logo);
        bckProgress = (ProgressBar) findViewById(R.id.background_progressbar);
        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
        }
        /*Setting title bar to show custom view*/
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.titlebar);

        String ID = getIntent().getExtras().getString("itemID");
        String url = getIntent().getExtras().getString("itemURL");
        int likes = getIntent().getExtras().getInt("likes");
        int dislikes = getIntent().getExtras().getInt("dislikes");

        // Sends ID and type to Fragment instance
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        String type = "A";
        LikeDislikeFragment fragmentLD = LikeDislikeFragment.newInstance(ID, type, likes, dislikes);
        ft.replace(R.id.like_dislike_fragment, fragmentLD);
        ft.commit();

        webView = (WebView) findViewById(R.id.webView);
        bckProgress.setVisibility(View.VISIBLE);
        bckImg.setVisibility(View.VISIBLE);
        bckView.setVisibility(View.VISIBLE);
        webView.setVisibility(View.INVISIBLE);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                String javaScript = "javascript:"
                        + "$(\".navbar\").hide();"
                        + "$(\".latestposts\").hide();";
                view.loadUrl(javaScript);
                bckView.setVisibility(View.INVISIBLE);
                bckImg.setVisibility(View.INVISIBLE);
                bckProgress.setVisibility(View.INVISIBLE);
                webView.setVisibility(View.VISIBLE);
            }
        });
        webView.loadUrl(url);

        logoButton = (ImageButton)findViewById(R.id.logo_button);
        logoButton.setOnClickListener(logo);
    }

    private View.OnClickListener logo = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
           finish();
        }
    };

}