package no.hig.fiber.fiberApp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Database Helper class.
 */
class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "fiber.db";
    private static final int DATABASE_VERSION = 1;

    /**
     * Sets up db helper.
     * @param ctx activity context
     */
    public DatabaseHelper(Context ctx) {
        super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Creates table
     * @param db database
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DatabaseItem.LIKE_DISLIKE_CREATE_TABLE);
    }

    /**
     * Does not contain any parameters yet, since it is only the first version.
     * @param db database
     * @param oldVersion old version number
     * @param newVersion new version number
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO How did our DB change? Have we added new column? Renamed the column?

    }

}
