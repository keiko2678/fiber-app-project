package no.hig.fiber.fiberApp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import static no.hig.fiber.fiberApp.ViewHolderClass.*;

/**
 * A custom ArrayAdapter class for setting item values in TextViews
 *
 * http://www.vogella.com/tutorials/AndroidListView/article.html
 * http://developer.android.com/training/improving-layouts/smooth-scrolling.html#ViewHolder
 * http://stackoverflow.com/questions/13164054/viewholder-good-practice
 */
class RadioActivityArrayAdapter extends ArrayAdapter<ArticleItem> {

    private final Context context;
    private ArrayList<String> values = null;

    /*
    private View itemInListView;
    private TextView title;
*/

    public RadioActivityArrayAdapter(Context context, int layoutResourceId, ArrayList values) {
        super(context, layoutResourceId, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View itemInListView = convertView;
        SmallViewHolder holder;

        if(itemInListView == null){
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemInListView = inflater.inflate(R.layout.array_adapter_radio_item_activity, parent, false);
            holder = new SmallViewHolder();
            holder.sTitle = (TextView) itemInListView.findViewById(R.id.title_in_list);
            itemInListView.setTag(holder);
        } else {
            holder = (SmallViewHolder) itemInListView.getTag();
        }
        holder.sTitle.setText(values.get(position));

        return itemInListView;

    }
}
