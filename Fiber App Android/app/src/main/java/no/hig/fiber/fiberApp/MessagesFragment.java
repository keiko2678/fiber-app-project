package no.hig.fiber.fiberApp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.util.ByteArrayBuffer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;



public class MessagesFragment extends FragmentActivity {
    private AppFinalStrings strings;
    private Context appContext;
    private String exceptionTagInLog;
    private String infoTagInLog;
    public MessagesFragment(Context context){
        strings = new AppFinalStrings(context);
        appContext = context;

        exceptionTagInLog = strings.exceptionTagInLog;
        infoTagInLog = strings.infoTagInLog;
    }

    /**
     * Task, responsible for downloading a RSS feed a given URL.
     * This task will execute the download asynchronously.
     */
    private class checkMessages extends AsyncTask<String, Integer, String> {
        String rssFeed;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * @param url string URL
         * @return xml string structure
         */
        @Override
        protected String doInBackground(String... url) {
            if (rssFeed == null) {
                URL tmpUrl = null;
                try {
                    tmpUrl = new URL(url[0]);
                } catch (MalformedURLException e) {
                    Log.d(exceptionTagInLog, e.getMessage());
                }
                try {
                    URLConnection urlCon;
                    if (tmpUrl != null) {
                        urlCon = tmpUrl.openConnection();

                        InputStream is = urlCon.getInputStream();
                        BufferedInputStream bis = new BufferedInputStream(is);

                        ByteArrayBuffer baf = new ByteArrayBuffer(262144);      // 256 KB
                        int current;
                        while ((current = bis.read()) != -1) {
                            baf.append((byte) current);
                        }
                        rssFeed = new String(baf.toByteArray(), "UTF8");
                    }
                } catch (UnsupportedEncodingException e) {
                    Log.d(exceptionTagInLog, e.getMessage());
                } catch (ClientProtocolException e) {
                    Log.d(exceptionTagInLog, e.getMessage());
                } catch (IOException e) {
                    Log.d(exceptionTagInLog, e.getMessage());
                }
                // return XML
            }
            return rssFeed;
        }


        @Override
        protected void onPostExecute(String RSS) {
            if (RSS != null ) {
                Document doc = null;
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db;
                InputSource is = new InputSource();

                try {
                    db = dbf.newDocumentBuilder();
                    is.setCharacterStream(new StringReader(RSS));
                    if (is != null) {
                        doc = db.parse(is);
                    }
                } catch (NullPointerException e) {
                    Log.d(exceptionTagInLog, "No Internet Connection");  // No internet connection to RSS
                } catch (ParserConfigurationException e) {
                    Log.d(exceptionTagInLog, e.getMessage());
                } catch (SAXException e) {
                    Log.d(exceptionTagInLog, e.getMessage());
                } catch (IOException e) {
                    Log.d(exceptionTagInLog, e.getMessage());
                }

                if (doc != null) {
                    doc.getDocumentElement().normalize();
                    NodeList itemNodes = doc.getElementsByTagName("message");  //---retrieve all the <item> nodes---

                    for (int i = 0; i < itemNodes.getLength(); i++) {    // Go through all the nodes to build items
                        Node itemNode = itemNodes.item(i);
                        if (itemNode.getNodeType() == Node.ELEMENT_NODE) {
                            Element messageElement = (Element) itemNode;

                            String messageTitle = messageElement.getElementsByTagName("title").item(0).getTextContent();
                            String type = messageElement.getElementsByTagName("type").item(0).getTextContent();
                            String messageText = messageElement.getElementsByTagName("text").item(0).getTextContent();
                            String url = messageElement.getElementsByTagName("text").item(0).getTextContent();
                            String timeStart = messageElement.getElementsByTagName("text").item(0).getTextContent();
                            String timeEnd = messageElement.getElementsByTagName("text").item(0).getTextContent();

                            AlertDialog.Builder builder = new AlertDialog.Builder(appContext);

                            // Add the buttons
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User clicked OK button
                                }
                            });
                            builder.setNegativeButton("Close", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // User cancelled the dialog
                                }
                            });

                            // Create the AlertDialog
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    }
                }
            }
        }
    }

}
