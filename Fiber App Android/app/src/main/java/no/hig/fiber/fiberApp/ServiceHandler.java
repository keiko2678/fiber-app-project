package no.hig.fiber.fiberApp;

import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
/**
 * ServiceHandler that handles http request against like and dislike api
 *
 * Borrowed some code from:
 * http://www.androidhive.info/2012/01/android-json-parsing-tutorial/
 */

class ServiceHandler {
    AppFinalStrings strings = null;
    private static String response = null;
    public final static int GET = 1;
    public final static int POST = 2;
    public final static int PUT = 3;

    /**
     * Empty constructor
     */
    public ServiceHandler(Context context) {
        strings = new AppFinalStrings(context);
    }
    /**
     * Making service call if no http parameters are sent
     * @url - url to make request
     * @method - http request method
     * */
    public String makeServiceCall(String url, int method) {
        return this.makeServiceCall(url, method, null);
    }

    /**
    * Making service call
    * @url - url to make request
    * @method - http request method, post, put or get
    * */
    public String makeServiceCall(String url, int method, List<NameValuePair> params) {
        try {
            // http client
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpEntity httpEntity;
            HttpResponse httpResponse = null;

            // Checking http request method type
            // POST
            if (method == POST) {
                HttpPost httpPost = new HttpPost(url);
                if (params != null) {
                    httpPost.setEntity(new UrlEncodedFormEntity(params));
                }
                httpResponse = httpClient.execute(httpPost);
            // GET
            } else if (method == GET) {
                HttpGet httpGet = new HttpGet(url);
                httpResponse = httpClient.execute(httpGet);
            // PUT
            } else if (method == PUT) {
                HttpPut httpPut = new HttpPut(url);
                if(params != null) {
                    httpPut.setEntity(new UrlEncodedFormEntity(params));
                }
                httpResponse = httpClient.execute(httpPut);
            }
            if (httpResponse != null) {
                httpEntity = httpResponse.getEntity();
                response = EntityUtils.toString(httpEntity);
            }

        } catch (UnsupportedEncodingException e) {
            Log.d(strings.exceptionTagInLog, e.getMessage());
        } catch (ClientProtocolException e) {
            Log.d(strings.exceptionTagInLog, e.getMessage());
        } catch (IOException e) {
            Log.d(strings.exceptionTagInLog, e.getMessage());
        }

        return response;

    }
}
