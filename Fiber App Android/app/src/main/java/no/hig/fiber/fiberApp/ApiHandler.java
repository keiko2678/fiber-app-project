package no.hig.fiber.fiberApp;

import android.content.Context;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Helps with comunicating with API
 * Created by idagranholt on 30/11/14.
 */
public class ApiHandler {
   AppFinalStrings strings = null;
    // Statics:
    private static final String TAG_LIKES = "likeCount";
    private static final String TAG_DISLIKES = "dislikeCount";

    // Urls:
    private String newUrl = null;
    private String updateURL = null;
    private String getURL = null;

   // Data:
    private final String ID;
    private final String type;
    public int likes;
    public int dislikes;
    private ServiceHandler sh = null;

    /**
     * Constructor
     * @param ID item id
     * @param type type of media
     */
    public ApiHandler(String ID, String type, Context context) {
        sh = new ServiceHandler(context);
        strings = new AppFinalStrings(context);
        newUrl = strings.fiberApiUrl +"/item/new";
        updateURL = strings.fiberApiUrl +"/item/update";
        getURL = strings.fiberApiUrl +"/item";


        this.ID = ID;
        this.type = type;
        this.likes = 0;
        this.dislikes = 0;

        getURL = getURL + "/" + this.ID + "/" + this.type + "/" + strings.apiVersionNumber;
    }

    /**
     * * Making get call to API.
     * If there was no match, makes a push call to API
     * and sends data for making a new item.
     * @return ApiHandler object with likes and dislikes data
     */
    public ApiHandler getLikeDislike() {
        // Gets like and dislike data from API
        String jsonStr = sh.makeServiceCall(getURL, ServiceHandler.GET);
        if(jsonStr != null) {
            try {
                // If no match - send post request with article data to API
                if (jsonStr.equals("\"no match\"")) {
                    jsonStr = this.newLikeDislike();
                }
                // Updates api object with new data
                JSONObject jsonObject = new JSONObject(jsonStr);
                this.likes = jsonObject.getInt(TAG_LIKES);
                this.dislikes = jsonObject.getInt(TAG_DISLIKES);

            } catch (JSONException e) {
                Log.d(strings.exceptionTagInLog, e.getMessage() );
            }
        }
       return this;
    }

    /**
     * Makes a new item in remote database trough API
     * This function is only called if getLikeDislike didn't find a matching item
     * @return json string with new item data
     */
    String newLikeDislike() {
        // List for sending data to ServiceHandler
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("itemId", this.ID));
        params.add(new BasicNameValuePair("type", this.type));
        params.add(new BasicNameValuePair("apiVersion", String.valueOf(strings.apiVersionNumber)));
        params.add(new BasicNameValuePair("likeCount", String.valueOf(this.likes)));
        params.add(new BasicNameValuePair("dislikeCount", String.valueOf(this.dislikes)));
        // Post request with data to API via ServiceHandler
        return  sh.makeServiceCall(newUrl, ServiceHandler.POST,params );
    }

    /**
     * Called if user pressed like button
     * Updates like and dislike count
     * @param likePushed 1 if the user has pushed like before this
     * @param dislikePushed 1 if the user has pushed dislike before this
     * @return updated api object
     */
    public ApiHandler updateLike( int likePushed, int dislikePushed) {
        String likeUpdate;
        String dislikeUpdate = "DD";
        // Updates the variable based on what the user has pushed before
        if(likePushed == 0) {
            likeUpdate = "L+";
        } else {
            likeUpdate = "L-";
        }
        if(dislikePushed == 1) {
            dislikeUpdate = "D-";
        }
        Log.d("API LIKEPUSHED: ", likeUpdate+" "+dislikeUpdate);
        return updateLikDislike(likeUpdate, dislikeUpdate);
    }

    /**
     * Called if user pressed dislike button
     * Updates like and dislike count
     * @param likePushed 1 if the user has pushed like before this
     * @param dislikePushed 1 if the user has pushed dislike before this
     * @return updated api object
     */
    public ApiHandler updateDislike( int likePushed, int dislikePushed) {
        String likeUpdate = "LL";
        String dislikeUpdate;
        // Updates the variable based on what the user has pushed before
        if(dislikePushed==0){
            dislikeUpdate = "D+";
        }else{
            dislikeUpdate = "D-";
        }
        if(likePushed==1){
            likeUpdate = "L-";
        }
        return updateLikDislike(likeUpdate, dislikeUpdate);
    }

    /**
     * Updates remote database with likes and dislikes data trough API
     * @param likeUpdate new number of likes
     * @param dislikeUpdate new number of dislikes
     * @return updated api object
     */
    ApiHandler updateLikDislike(String likeUpdate, String dislikeUpdate) {
        String newUpdateURL = updateURL + "/" + likeUpdate + "/" + dislikeUpdate;
        // List for sending data to ServiceHandler
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("itemId", this.ID));
        params.add(new BasicNameValuePair("type", this.type));
        params.add(new BasicNameValuePair("apiVersion", String.valueOf(strings.apiVersionNumber)));

        // Updates api via ServiceHandler
        String jsonStr = sh.makeServiceCall(newUpdateURL, ServiceHandler.PUT,params );
        // Updates api object with new data

        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject(jsonStr);
            this.likes = jsonObject.getInt(TAG_LIKES);
            this.dislikes = jsonObject.getInt(TAG_DISLIKES);
        } catch (JSONException e) {
            Log.d(strings.exceptionTagInLog, e.getMessage() );
        }
        return this;
    }
}
