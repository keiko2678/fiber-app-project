package no.hig.fiber.fiberApp;

import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * View Holder class for ArrayAdapters, making scrolling smoother
 *
 * http://developer.android.com/training/improving-layouts/smooth-scrolling.html#ViewHolder
 * http://stackoverflow.com/questions/13164054/viewholder-good-practice
 */
public class ViewHolderClass {
    public static class ViewHolder {
        public TextView vTitle;
        public TextView vDate;
        public TextView vLikes;
        public TextView vDislikes;
        public ImageView vImage;
    }
    public static class SmallViewHolder {
        public TextView sTitle;
    }
}

