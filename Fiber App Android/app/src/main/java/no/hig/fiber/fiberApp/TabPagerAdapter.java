package no.hig.fiber.fiberApp;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/*Using code from http://www.learn2crack.com/2013/12/android-swipe-view-tab-layout-example.html*/
class TabPagerAdapter extends FragmentStatePagerAdapter {
    private Fragment tabVideo = null;
    private Fragment tabArticle = null;
    //private Fragment tabRadio = null;

    public TabPagerAdapter(FragmentManager fm) {
        super(fm);

    }
    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0:
                if(tabVideo == null)
                    return tabVideo = new TabVideo();
                else
                    return tabVideo;
            case 1:
                if(tabArticle == null)
                    return tabArticle = new TabArticles();
                else
                    return tabArticle;
           /* case 2:
                if(tabRadio == null)
                    return tabRadio =new TabRadio();
                else
                    return tabRadio;
           */
        }
        return null;
    }
    @Override
    public int getCount() {
        return 2; //No of Tabs   - MUST BE SET TO 3 FOR RADIO
    }
}