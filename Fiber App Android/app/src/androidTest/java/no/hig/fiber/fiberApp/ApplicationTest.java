package no.hig.fiber.fiberApp;

import android.app.Application;
import android.test.ApplicationTestCase;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * We have very few actual data handler and data logic operations
 * that does not use internt connection in order to fill upp the logic in the class.
 *
 *
 */
public class ApplicationTest extends ApplicationTestCase<Application> {

    public ApplicationTest() {
        super(Application.class);


        TestItemTypes();
        TestFinalStrings();
    }

    public void TestFinalStrings(){
        AppFinalStrings testFinalStrings = new AppFinalStrings(this.getContext());

            // Checking that the tags for the Database is as in documentation
        assertEquals(testFinalStrings.dbTypeArticle,"A");
        assertEquals(testFinalStrings.dbTypeVideo,"V");
        assertEquals(testFinalStrings.dbTypeRadio,"P");

            // Checking that the number of items showd in the tablists is at least 10
        if(testFinalStrings.noOfItemsInTabLists > 10){
            assert true;
        }else {
            assert false;
        }

            // if the links are working links;
        testURL(testFinalStrings.fiberWordpressRssUrl);
        testURL(testFinalStrings.fiberRadioPodcastRssUrl);
        testURL(testFinalStrings.youtubeFiberTVJsonListUrl);
    }

    /*
     * Takes a string and checks if it can be converted to a URL and
     * if it is possible to open a connection against the url.
     */
    private void testURL (String url) {
        URL tmpUrl = null;
        try {
            tmpUrl = new URL(url);
        } catch (MalformedURLException e) {
            assert false;
        }

        try {
            tmpUrl.openConnection();
        } catch (IOException e) {
            assert false;
        }
    }

        // Checking that the itemtypes are different
    public void TestItemTypes(){
        AppFinalStrings testFinalString = new AppFinalStrings(this.getContext());
        ArticleItem tmpArticleItem = new ArticleItem(this.getContext());
        VideoItem tmpVideoItem = new VideoItem();
        RadioItem tmpRadioItem = new RadioItem();

            // Checking that the items are different
        assertNotSame(tmpArticleItem,tmpVideoItem);
        assertNotSame(tmpArticleItem,tmpRadioItem);

            // Checking that it gets the strings right;
        assertEquals(tmpArticleItem.strings.fiberWordpressRssUrl, testFinalString.fiberWordpressRssUrl);
        assertEquals(tmpRadioItem.strings.fiberRadioPodcastRssUrl, testFinalString.fiberRadioPodcastRssUrl);
    }
}