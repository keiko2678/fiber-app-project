# FIBER -app Project #

This is a project by: Ida, Per Christian, Therese and Reza.
The project is made for the student Newspaper FIBER and started out as a project in the course IMT3672 at GUC.

## This project is run on two different platforms ##

This repo contains the working sets for the android version as well as the API backend, remote config and graphics. There is a separate repo for the iOS app.

Read more updated information on our [wikipage. ](https://bitbucket.org/keiko2678/fiber-app-project/wiki/Home)